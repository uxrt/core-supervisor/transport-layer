# uxrt-transport-layer

This is the IPC transport layer for UX/RT. It provides a Unix-like read()/write() API on top of seL4 IPC.

## Status

IPC channel and notification FDs are mostly complete and working, although polling is still unimplemented.
