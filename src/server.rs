// Copyright 2022-2024 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use core::mem::size_of;
use core::cell::Cell;

use sel4::{
	Endpoint,
	LONG_IPC_GET_RECV_TOTAL_SIZE,
	LONG_IPC_GET_REPLY_SIZE,
	LONG_IPC_OPTIONAL,
	RecvLongMsgBuffer,
	RecvToken,
	Reply,
	SendLongMsgBuffer,
	ToCap,
	long_reply_then_recv,
};

use sel4_sys::seL4_GetIPCBuffer;

use crate::{
	AccessMode,
	BaseFileDescriptor,
	BufferArray,
	FileDescriptor,
	IOError,
	FixedIPCBuffer,
	HEADER_SIZE,
	MsgHeader,
	MsgType,
	Offset,
	OffsetType,
	POLLIN,
	RawFileDescriptor,
	RawMsgType,
	RawOffsetType,
	SRV_ERR_INVAL,
	SRV_ERR_SRCH,
	ServerStatus,
	convert_to_offset_result,
	debug_println,
	is_clunk_err,
	get_long_recv_buf,
	get_long_send_buf,
	get_md,
};

use crate::channel::{
	BaseChannelFileDescriptor,
	ClientBadge,
	ClientLabel,
	FIXED_BUF_MAX_SIZE,
	RESERVED_REGS,
	OFFSET_IDX,
	ReadBufType,
	SIZE_IDX,
	ServerLabel,
	get_primary_buffer,
	get_secondary_buffer,
	get_send_word_size,
};

#[thread_local]
static DEFAULT_MD: Cell<RawFileDescriptor> = Cell::new(-1);

#[thread_local]
static mut HEADER: MsgHeader = MsgHeader::new();

///Options for server channel file descriptors
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct ServerChannelOptions(usize);

impl ServerChannelOptions {
	///Create a new (empty) `ServerChannelOptions`
	#[inline]
	pub fn new() -> Self {
		Self(0)
	}
	///Returns self, adding the option to get the total size of the buffer
	///sent by the client regardless of the receive buffer
	#[inline]
	pub fn get_recv_total_size(&self) -> Self {
		Self(self.0 | LONG_IPC_GET_RECV_TOTAL_SIZE)
	}
	///Returns self, adding the option to get the size of the client's reply
	///buffer
	#[inline]
	pub fn get_reply_size(&self) -> Self {
		Self(self.0 | LONG_IPC_GET_REPLY_SIZE)
	}
}

///Sets the thread-local default message descriptor to use with read variants
///that don't accept one (if it is unset, any such reads will fail)
pub fn setmd(md: RawFileDescriptor){
	DEFAULT_MD.set(md);
}

///A server-side file descriptor
#[derive(Clone)]
#[repr(C)]
pub struct ServerChannelFileDescriptor {
	base_fd: BaseChannelFileDescriptor,
}

impl ServerChannelFileDescriptor {
	///Internal constructor (UnifiedFileDescriptor::new_server_channel is
	///the public constructor)
	///
	///The access mode specifies what kind of accesses will be accepted
	///from a client, not what kinds of calls the server itself will be
	///allowed to make, since servers must always use both reads (to
	///accept messages) and writes (to reply to them).
	///
	pub(crate) fn new(endpoint: Endpoint, access: AccessMode, options: ServerChannelOptions) -> ServerChannelFileDescriptor {
		let base_fd = BaseChannelFileDescriptor::new(access, None, None, None, Some(endpoint), options.0);
		ServerChannelFileDescriptor {
			base_fd,
		}
	}
	///Internal method to send an error reply on read failures
	fn mread_error_reply(&self, errno: ServerStatus, ret: IOError, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		debug_println!("mread_error_reply: {}", errno);
		if let Err(err) = get_md(md).mwpwriteb(0, 0, OffsetType::End, -(errno as ServerStatus)){
			Err(err)
		}else{
			Err(ret)
		}
	}
	///Internal method called to handle m*read*() errors
	fn mread_err_end(&self, header: &mut MsgHeader, err: sel4::Error) -> Result<(usize, usize), IOError> {
		debug_println!("mread_err_end: initializing header");
		if is_clunk_err(&err) {
			debug_println!("error is clunk: {:?}", err);
			header.msgtype = MsgType::Clunk as RawMsgType;
			header.whence = OffsetType::Start as RawOffsetType;
			header.offset = 0;
			header.client_write_size = 0;
			header.client_read_size = 0;
			header.client_id = -1;
			header.badge = 0;
			Ok((0, 0))
		}else{
			Err(IOError::SyscallError(err))
		}
	}
	///Internal method to unpack the badge and label into individual fields
	fn unpack_msg_info(&self, badge: usize, label: usize) -> Result<(MsgType, RawFileDescriptor, u32, OffsetType, usize, usize, bool), (Offset, IOError)> {
		let (msgtype, client_id, user_badge) = ClientBadge::unpack(badge)?;
		let (whence, reply_short_size, recv_short_remainder, has_footer) = ClientLabel::unpack(label)?;
		Ok((msgtype, client_id, user_badge, whence, reply_short_size, recv_short_remainder, has_footer))
	}
	///Internal implementation of the post-syscall part of m*read* methods
	fn mread_end(&self, header: &mut MsgHeader, msg: RecvToken, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		debug_println!("mread_end");
		debug_println!("{:p} {}", header, md);
		let res = self.unpack_msg_info(msg.badge, msg.label);
		if let Ok((msgtype, client_id, user_badge, whence, reply_short_size, recv_short_remainder, has_footer)) = res {
			let mut recv_word_size = msg.words_transferred();

			let base_buf = unsafe { &mut*(seL4_GetIPCBuffer()) };
			let offset = if has_footer {
				recv_word_size -= RESERVED_REGS;
				base_buf.msg[recv_word_size + OFFSET_IDX]
			}else{
				0
			};
			if recv_word_size > 0 && recv_short_remainder > 0 {
				recv_word_size -= 1;
			}
			debug_println!("recv_word_size: {} recv_short_remainder: {}, has_footer: {}, offset: {}", recv_word_size, recv_short_remainder, has_footer, offset);
			let recv_short_size = recv_word_size * size_of::<usize>() + recv_short_remainder;
			if recv_short_size > FIXED_BUF_MAX_SIZE {
				return self.mread_error_reply(SRV_ERR_INVAL, IOError::MessageTooLong, md);
			}

			let recv_long_size = msg.recv_len();
			let recv_long_total_size = msg.recv_total_len();
			let reply_long_size = msg.recv_reply_len();

			debug_println!("recv_long_size: {} recv_long_total_size: {} reply_long_size: {}", recv_long_size, recv_long_total_size, reply_long_size);


			debug_println!("initializing header");
			header.msgtype = msgtype as RawMsgType;
			header.whence = whence as RawOffsetType;
			header.offset = offset as Offset;
			if recv_long_total_size > 0 || reply_long_size > 0 {
				header.client_write_size = recv_long_total_size;
				header.client_read_size = reply_long_size;
			}else{
				header.client_write_size = recv_short_size;
				header.client_read_size = reply_short_size;
			}
			header.badge = user_badge;
			if msgtype == MsgType::Clunk {
				header.client_id = base_buf.msg[0] as RawFileDescriptor;
			}else{
				header.client_id = client_id;
			}
			debug_println!("header done");
			Ok((recv_short_size, recv_long_size))
		}else{
			let (errno, err) = res.unwrap_err();
			let _ = self.mread_error_reply(errno as ServerStatus, err, md);
			Err(err)
		}
	}
	///Gets the endpoint and reply capabilities to use in a syscall
	fn get_caps(&self, md: RawFileDescriptor) -> Result<(Endpoint, Reply), IOError> {
		let reply = if let Some(r) = get_md(md).get_reply() {
			r
		}else{
			return Err(IOError::InvalidOperation);
		};

		Ok((self.base_fd.get_server_endpoint(), reply))
	}
	///Internal method implementing polling reads
	fn poll_common(&self, flags: Offset) -> Result<(usize, Offset), IOError>{
		if flags & POLLIN == 0 {
			return Err(IOError::InvalidOperation);
		}
		let endpoint = self.base_fd.get_server_endpoint();
		if let Err(err) = endpoint.poll() {
			Err(IOError::SyscallError(err))
		}else{
			Ok((0, 0))
		}
	}
	///Internal method implementing core functionality of standalone
	///mread* methods
	fn mread_standalone_common(&self, bufs: &mut [RecvLongMsgBuffer], header: &mut MsgHeader, md: RawFileDescriptor, long_optional: bool) -> Result<(usize, usize), IOError> {
		let (endpoint, reply) = self.get_caps(md)?;
		let mut flags = self.base_fd.get_flags();
		if long_optional {
			flags &= LONG_IPC_OPTIONAL as u64;
		}
		debug_println!("mread_standalone_common: {}", flags);
		match endpoint.long_recv(reply, bufs, flags as usize) {
			Ok(msg) => self.mread_end(header, msg, md),
			Err(err) => {
				self.mread_err_end(header, err)
			},
		}

	}
	///Internal method called at the start of m*read() methods to check
	///the buffer length and set up the vector to be passed to m*readv()
	fn mread_start(&self, buf: &mut [u8]) -> Result<[RecvLongMsgBuffer; 2], IOError> {
		if buf.len() < HEADER_SIZE {
			return Err(IOError::InvalidArgument);
		}
		let header_slice = &buf[..HEADER_SIZE];
		let data_slice = &buf[HEADER_SIZE..];
		unsafe {
			Ok([
				RecvLongMsgBuffer::new_raw(header_slice.as_ptr() as *mut u8, HEADER_SIZE),
				RecvLongMsgBuffer::new_raw(data_slice.as_ptr() as *mut u8, data_slice.len()),
			])
		}
	}
	///Internal method called at the start of m*readv() methods to check
	///the size of the first buffer.
	fn mreadv_start<'a>(&'a self, bufs: &'a mut [RecvLongMsgBuffer]) -> Result<(), IOError> {
		if bufs.len() == 0 || bufs[0].len != HEADER_SIZE {
			Err(IOError::InvalidArgument)
		}else{
			Ok(())
		}
	}
	///Internal method called at the start of m*readb() methods to check
	///the read size.
	fn mreadb_start(&self, size: usize) -> Result<(), IOError> {
		if size < FIXED_BUF_MAX_SIZE {
			Err(IOError::InvalidArgument)
		}else{
			Ok(())
		}
	}
	///Internal method implementing the core functionality of mpwriteread*
	///methods
	fn mpwriteread_common(&self, w_bufs: &[SendLongMsgBuffer], r_bufs: &mut [RecvLongMsgBuffer], status: ServerStatus, w_len: usize, offset: Offset, header: &mut MsgHeader, md: RawFileDescriptor, long_optional: bool) -> Result<(usize, usize), IOError> {
		let (endpoint, reply) = self.get_caps(md)?;
		let (label, short_len, send_bufs) = mpwrite_reply_start(status, w_len, offset, w_bufs)?;
		let mut flags = self.base_fd.get_flags();
		if long_optional {
			flags &= LONG_IPC_OPTIONAL as u64;
		}
		debug_println!("mpwriteread_common: {}", flags);
		match long_reply_then_recv(reply, label, short_len, endpoint, send_bufs.unwrap_or(&[]), r_bufs, flags as usize) {
			Ok(msg) => self.mread_end(header, msg, md),
			Err(err) => {
				self.mread_err_end(header, err)
			},
		}
	}

}

impl FileDescriptor for ServerChannelFileDescriptor {
	#[inline]
	fn default_read_update_offset_type(&self) -> OffsetType {
		OffsetType::Start
	}
	#[inline]
	fn default_write_update_offset_type(&self) -> OffsetType {
		OffsetType::Start
	}
	#[inline]
	fn get_access(&self) -> AccessMode {
		self.base_fd.get_access()
	}
	fn getbuf_r(&self) -> Option<FixedIPCBuffer> {
		if let Some(mut buf) = self.base_fd.getbuf_r() {
			buf.header = unsafe { BufferArray::new(&mut HEADER as *mut MsgHeader as *mut u8, HEADER_SIZE) };
			Some(buf)
		}else{
			None
		}
	}
	fn getbuf_w(&self) -> Option<FixedIPCBuffer> {
		self.base_fd.getbuf_w()
	}
	fn mreadb(&self, size: usize, md: RawFileDescriptor) -> Result<usize, IOError>{
		if size > FIXED_BUF_MAX_SIZE {
			return Err(IOError::InvalidArgument);
		}
		let mut fixed_buf = get_secondary_buffer().get_data();
		let mut long_bufs = get_long_recv_buf(&mut fixed_buf, size)?;
		self.mreadb_start(size)?;
		let mut header = MsgHeader::default();
		let (short_size, long_size) = self.mread_standalone_common(&mut long_bufs, &mut header, md, true)?;
		unsafe { header.to_raw_buf(&mut HEADER) };
		let size = self.base_fd.handle_fixed_read_bufs(short_size, long_size);
		Ok(size)
	}
	fn mread(&self, buf: &mut [u8], md: RawFileDescriptor) -> Result<usize, IOError> {
		let mut bufs = self.mread_start(buf)?;
		self.mreadv(&mut bufs, md)
	}
	fn mreadv(&self, bufs: &mut [RecvLongMsgBuffer], md: RawFileDescriptor) -> Result<usize, IOError> {
		if bufs.len() == 0 || bufs[0].len != size_of::<MsgHeader>() {
			return Err(IOError::InvalidArgument);
		}
		self.mreadv_start(bufs)?;
		let mut header = MsgHeader::default();
		let (_, long_size) = self.mread_standalone_common(&mut bufs[1..], &mut header, md, false)?;
		//this should never fail since the conditions were checked at
		//the beginning
		let _ = unsafe { header.to_long_buf(bufs) };
		self.base_fd.set_read_type(ReadBufType::Unset);
		debug_println!("size: {} header size: {}", size, size_of::<MsgHeader>());
		Ok(long_size + size_of::<MsgHeader>())
	}
	fn wpreadv(&self, bufs: &mut [RecvLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {

		match whence {
			OffsetType::Current => {
				if offset != 0 {
					return Err(IOError::InvalidArgument);
				}
				let md = DEFAULT_MD.get();
				convert_to_offset_result(self.mreadv(bufs, md))
			},
			OffsetType::Poll => {
				self.poll_common(offset)
			},
			_ => Err(IOError::InvalidArgument)
		}
	}
	fn wpreadb(&self, size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		match whence {
			OffsetType::Current => {
				if offset != 0 {
					return Err(IOError::InvalidArgument);
				}
				let md = DEFAULT_MD.get();
				convert_to_offset_result(self.mreadb(size, md))
			},
			OffsetType::Poll => {
				self.poll_common(offset)
			},
			_ => Err(IOError::InvalidArgument)
		}
	}
	fn mwpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
		let md = get_md(DEFAULT_MD.get());
		if offset != 0 || whence != OffsetType::Current || md.get_reply().is_none() {
			Err(IOError::InvalidArgument)
		}else{
			md.mwpwritev(bufs, offset, whence, status)
		}
	}
	fn mwpwriteb(&self, size: usize, offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
		let md = get_md(DEFAULT_MD.get());
		if offset != 0 || whence != OffsetType::Current || md.get_reply().is_none() {
			Err(IOError::InvalidArgument)
		}else{
			md.mwpwriteb(size, offset, whence, status)
		}
	}
	fn wpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		self.mwpwritev(bufs, offset, whence, 0)
	}
	fn wpwriteb(&self, size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		self.mwpwriteb(size, offset, whence, 0)
	}
	fn wpwritereadv(&self, w_bufs: &[SendLongMsgBuffer], r_bufs: &mut [RecvLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, usize), IOError> {
		if offset != 0 || whence.base() != OffsetType::Start || whence.base() != OffsetType::Current {
			return Err(IOError::InvalidArgument);
		}
		let md = DEFAULT_MD.get();
		self.mpwritereadv(w_bufs, 0, r_bufs, 0, md)
	}
	fn wpwritereadb(&self, w_len: usize, r_len: usize, offset: Offset, whence: OffsetType) -> Result<(usize, usize), IOError> {
		if offset != 0 || whence.base() != OffsetType::Start || whence.base() != OffsetType::Current {
			return Err(IOError::InvalidArgument);
		}
		let md = DEFAULT_MD.get();
		self.mpwritereadb(w_len, 0, r_len, 0, md)
	}
	fn mpwriteread(&self, w_buf: &[u8], w_offset: Offset, r_buf: &mut [u8], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		let mut r_bufs = self.mread_start(r_buf)?;
		let w_bufs = [SendLongMsgBuffer::new(w_buf)];
		self.mpwritereadv(&w_bufs, w_offset, &mut r_bufs, status, md)
	}
	fn mpwritereadv(&self, w_bufs: &[SendLongMsgBuffer], w_offset: Offset, r_bufs: &mut [RecvLongMsgBuffer], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		if r_bufs.len() == 0 || r_bufs[0].len != size_of::<MsgHeader>() {
			return Err(IOError::InvalidArgument);
		}
		self.mreadv_start(r_bufs)?;
		let mut header = MsgHeader::default();
		let (_, long_size) = self.mpwriteread_common(w_bufs, &mut r_bufs[1..], status, 0, w_offset, &mut header, md, false)?;
		self.base_fd.set_read_type(ReadBufType::Unset);
		//this should never fail since the conditions were checked at
		//the beginning
		let _ = unsafe { header.to_long_buf(r_bufs) };
		Ok((0, long_size + size_of::<MsgHeader>()))
	}
	fn mpwritereadb(&self, w_len: usize, w_offset: Offset, r_len: usize, status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		let w_buf = get_primary_buffer().get_data();
		let w_long_bufs = get_long_send_buf(&w_buf, w_len)?;

		let mut r_buf = get_secondary_buffer().get_data();
		let mut r_long_bufs = get_long_recv_buf(&mut r_buf, r_len)?;
		self.mreadb_start(r_len)?;
		let mut header = MsgHeader::default();
		let (short_size, long_size) = self.mpwriteread_common(&w_long_bufs, &mut r_long_bufs, status, w_len, w_offset, &mut header, md, true)?;
		unsafe { header.to_raw_buf(&mut HEADER) };
		let size = self.base_fd.handle_fixed_read_bufs(short_size, long_size);

		Ok((0, size))
	}

}

///Internal function to set up a reply for mpwrite*() methods.
fn mpwrite_reply_start<'a>(status: ServerStatus, s_send_size: usize, offset: Offset, bufs: &'a [SendLongMsgBuffer]) -> Result<(usize, usize, Option<&'a [SendLongMsgBuffer<'a>]>), IOError> {
	if status >= 0 {
		debug_println!("mpwrite_reply_start: status: {}", status);
		let (mut send_word_size, has_footer) = get_send_word_size(s_send_size as usize, offset, status)?;
		debug_println!("send_word_size: {} has_footer: {}", send_word_size, has_footer);
		debug_println!("send_word_size: {}", send_word_size);
		if has_footer {
			let sys_buf = unsafe { &mut*(seL4_GetIPCBuffer()) };
			sys_buf.msg[send_word_size + SIZE_IDX] = status as usize;
			sys_buf.msg[send_word_size + OFFSET_IDX] = offset as usize;
			send_word_size += RESERVED_REGS;
			Ok((ServerLabel::pack(0, 0, s_send_size % size_of::<usize>(), true), send_word_size, Some(bufs)))
		}else{
			Ok((ServerLabel::pack(0, status as usize, s_send_size % size_of::<usize>(), false), send_word_size, Some(bufs)))
		}
	}else{
		if offset != 0 || bufs.len() > 0 && bufs[0].len > 0 || s_send_size > 0{
			Err(IOError::InvalidArgument)
		}else{
			Ok((ServerLabel::pack_err(-status as u32), 0, None))
		}
	}
}

///A server message descriptor
#[derive(Debug, Clone)]
pub struct ServerMessageDescriptor {
	base_fd: BaseFileDescriptor,
}

impl ServerMessageDescriptor {
	///Internal constructor (UnifiedFileDescriptor::new_server_message()
	///is the public constructor)
	pub(crate) fn new(reply: Reply) -> ServerMessageDescriptor {
		ServerMessageDescriptor {
			base_fd: BaseFileDescriptor::new(0, 0, 0, 0, reply.to_cap(), 0, AccessMode::ReadWrite, 0),
		}
	}
	///Internal implementation of common functionality for `mpwritev` and
	///`mwpwriteb`
	fn mwpwrite_common(&self, bufs: &[SendLongMsgBuffer], status: ServerStatus, w_len: usize, offset: Offset, whence: OffsetType, long_optional: bool) -> Result<usize, IOError>{
		debug_println!("mwpwrite_common");
		match whence {
			OffsetType::End => {
				debug_println!("end");
				let (label, short_len, send_bufs) = mpwrite_reply_start(status, w_len, offset, bufs)?;
				#[cfg(feature = "debug_msgs")]
				for buf in send_bufs.unwrap_or(&[]) {
					debug_println!("send buf len: {}", buf.len());
				}
				debug_println!("w_len: {} label: {:x} short_len: {}", w_len, label, short_len);
				debug_println!("reply cap: {}", self.get_reply().unwrap().to_cap());
				let flags = if long_optional {
					LONG_IPC_OPTIONAL
				}else{
					0
				};
				if let Err(err) = self.get_reply().unwrap().long_send(label, short_len, send_bufs.unwrap_or(&[]), flags) {
					debug_println!("reply failed");
					Err(IOError::SyscallError(err))
				}else{
					debug_println!("reply succeeded");
					Ok(w_len)
				}
			},
			OffsetType::Start => {
				debug_println!("start");
				#[cfg(feature = "debug_msgs")]
				for buf in bufs {
					debug_println!("send buf len: {}", buf.len());
				}
				match self.get_reply().unwrap().write_long_buf(bufs, offset as usize) {
					Ok(len) => Ok(len),
					Err(err) => Err(IOError::SyscallError(err)),
				}
			},
			_ => {
				Err(IOError::InvalidArgument)
			}
		}
	}
}

impl FileDescriptor for ServerMessageDescriptor {
	#[inline]
	fn default_read_offset_type(&self) -> OffsetType {
		OffsetType::Start
	}
	#[inline]
	fn default_read_update_offset_type(&self) -> OffsetType {
		OffsetType::Start
	}
	#[inline]
	fn default_write_offset_type(&self) -> OffsetType {
		OffsetType::End
	}
	#[inline]
	fn default_write_update_offset_type(&self) -> OffsetType {
		OffsetType::End
	}
	fn get_reply(&self) -> Option<Reply> {
		Some(self.base_fd.get_reply())
	}
	fn clunk(&self) {
		let label = ServerLabel::pack_err(SRV_ERR_SRCH as u32);
		let _ = self.get_reply().unwrap().send(label, 0, 0);
	}
	fn getbuf_w(&self) -> Option<FixedIPCBuffer> {
		Some(get_primary_buffer())
	}
	fn wpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		self.mwpwritev(bufs, offset, whence, 0)
	}
	fn wpwriteb(&self, size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		self.mwpwriteb(size, offset, whence, 0)
	}
	fn mwpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
		convert_to_offset_result(self.mwpwrite_common(bufs, status, 0, offset, whence, false))
	}
	fn mwpwriteb(&self, len: usize, offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
		let buf = get_primary_buffer().get_data();
		let long_bufs = get_long_send_buf(&buf, len)?;
		convert_to_offset_result(self.mwpwrite_common(&long_bufs, status, len, offset, whence, false))
	}
	fn wpreadv(&self, bufs: &mut [RecvLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		if whence != OffsetType::Start {
			return Err(IOError::InvalidArgument);
		};
		match self.get_reply().unwrap().read_long_buf(bufs, offset as usize) {
			Ok(len) => Ok((len, offset)),
			Err(err) => Err(IOError::SyscallError(err)),
		}
	}

}
