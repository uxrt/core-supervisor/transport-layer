// Copyright 2023-2024 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use core::mem::size_of;

use sel4::{
	Notification,
	RecvLongMsgBuffer,
	SendLongMsgBuffer,
	ToCap,
};

#[allow(unused)]
use crate::debug_println;

use crate::{
	AccessMode,
	BaseFileDescriptor,
	BufferArray,
	FileDescriptor,
	IOError,
	FixedIPCBuffer,
	Offset,
	OffsetType,
	ServerStatus,
};

pub const NOTIFICATION_MSG_SIZE: usize = size_of::<usize>();

#[thread_local]
static mut RES_BUFFER: [usize; 1] = [0];

///A notification file descriptor.
#[derive(Clone)]
#[repr(C)]
pub struct NotificationFileDescriptor {
	base_fd: BaseFileDescriptor
}

impl NotificationFileDescriptor {
	///Internal constructor (UnifiedFileDescriptor::Notification is the
	///public one).
	pub fn new(notification: Notification, access: AccessMode) -> NotificationFileDescriptor {
		NotificationFileDescriptor {
			base_fd: BaseFileDescriptor::new(0, 0, 0, 0, 0, notification.to_cap(), access, 0),
		}
	}
}

impl FileDescriptor for NotificationFileDescriptor {
	#[inline]
	fn get_access(&self) -> AccessMode {
		self.base_fd.get_access()
	}
	fn getbuf_w(&self) -> Option<FixedIPCBuffer> {
		Some(FixedIPCBuffer {
			header: BufferArray::null(),
			data: unsafe { BufferArray::new(RES_BUFFER.as_mut_ptr() as *mut u8, RES_BUFFER.len()) },
		})
	}
	fn clunk(&self) {
		self.seterrno(ServerStatus::MAX);
	}
	fn seterrno(&self, errno: ServerStatus) {
		self.base_fd.set_errno(errno);
	}
	fn wpreadv(&self, bufs: &mut [RecvLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		if whence.base() != OffsetType::Current || offset != 0 || bufs.len() == 0 || bufs[0].len < NOTIFICATION_MSG_SIZE {
			return Err(IOError::InvalidArgument);
		}
		let res = self.wpreadb(NOTIFICATION_MSG_SIZE, 0, OffsetType::Current);
		if res.is_ok(){
			let short_buf = self.getbuf_w().unwrap();
			bufs[0][0..NOTIFICATION_MSG_SIZE].copy_from_slice(&short_buf.get_data()[0..NOTIFICATION_MSG_SIZE]);
		}
		res
	}
	fn wpreadb(&self, size: usize, offset: Offset, whence: OffsetType) ->   Result<(usize, Offset), IOError> {
		if whence.base() != OffsetType::Current || offset != 0 {
			return Err(IOError::InvalidArgument);
		}
		if size < NOTIFICATION_MSG_SIZE {
			return Err(IOError::InvalidArgument)
		}
		let res = self.base_fd.get_notification().wait();
		unsafe { RES_BUFFER[0] = res };
		let errno = self.base_fd.get_errno();
		if errno == 0 {
			Ok((NOTIFICATION_MSG_SIZE, 0))
		}else if errno == ServerStatus::MAX {
			Ok((0, 0))
		}else{
			Err(IOError::ServerError(errno))
		}


	}
	fn wpwritev(&self, _bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		if whence.base() != OffsetType::Current || offset != 0 {
			return Err(IOError::InvalidArgument);
		}
		self.wpwriteb(0, 0, OffsetType::Current)
	}
	fn wpwriteb(&self, _size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		if whence.base() != OffsetType::Current || offset != 0 {
			return Err(IOError::InvalidArgument);
		}

		let errno = self.base_fd.get_errno();
		if errno == 0 {
			self.base_fd.get_notification().signal();
			unsafe { RES_BUFFER[0] = 0; }
			Ok((NOTIFICATION_MSG_SIZE, 0))
		}else if errno == ServerStatus::MAX {
			return Ok((0, 0));
		}else{
			return Err(IOError::ServerError(errno));
		}
	}
}
