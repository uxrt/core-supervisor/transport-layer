// Copyright 2022-2024 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use core::cell::Cell;
use core::mem::size_of;
use core::ptr::copy_nonoverlapping;
use core::convert::TryFrom;

use num_traits::FromPrimitive;
use bilge::prelude::*;
use zerocopy::{
	AsBytes,
	FromBytes,
};

use sel4_sys::{
	seL4_GetIPCBuffer,
	seL4_MsgMaxLength,
	seL4_NumErrors,
};

use sel4::{
	Endpoint,
	FromCap,
	RecvLongMsgBuffer,
	ToCap,
};

use crate::{
	AccessMode,
	BaseFileDescriptor,
	BufferArray,
	FixedIPCBuffer,
	IOError,
	Offset,
	OffsetType,
	SRV_ERR_INVAL,
	SRV_ERR_NOSYS,
	ServerStatus,
	debug_println,
};

pub const FIXED_BUF_MAX_SIZE: usize = (seL4_MsgMaxLength as usize - RESERVED_REGS) * size_of::<usize>();
pub const HEADER_SIZE: usize = size_of::<MsgHeader>();

#[cfg(target_pointer_width = "64")]
pub type RawMsgType = u32;
#[cfg(target_pointer_width = "64")]
pub type RawOffsetType = u32;
pub type RawOffset = i64;

///Internal enum to specify the read buffer type
#[derive(Copy, Clone, Debug)]
pub(crate) enum ReadBufType {
	Unset,
	Primary,
	Secondary,
}

#[thread_local]
pub(crate) static mut SECONDARY_BUFFER: [u8; FIXED_BUF_MAX_SIZE] = [0; FIXED_BUF_MAX_SIZE];
#[thread_local]
static READ_BUF_TYPE: Cell<ReadBufType> = Cell::new(ReadBufType::Unset);

///Message type code (returned in the status buffer for server-side FDs)
#[derive(Clone, Copy, Debug, Primitive, PartialEq)]
pub enum MsgType {
	Clunk = 0,
	Read = 1,
	Write = 2,
	WriteRead = 3,
}

///Header for messages read from server channels
///
///For messages read with *read(), this is prepended to the buffer. With *readv,
///this is placed in the first buffer. With *readb(), this is in a separate
///member of the FixedIPCBuffer struct.
///
///Fields:
///
///msgtype:             Raw message type (to get it as a MsgType enum, call the
///                     msgtype_enum() method)
///whence:              Raw offset type (to get it as a MsgType enum, call the
///                     whence_enum() method)
///offset:              Requested offset (this is passed directly from the
///                     client; the transport layer doesn't store an offset).
///client_write_size:   Total size of the client's write buffer, regardless of
///                     the server read buffer used to read the message.
///client_read_size:    Total size of the client's read buffer
///client_id:		ID of the client file description that sent this
///			message. This is an arbitrary ID that is only unique
///			among clients on a particular file description.
///badge:               The badge of the client file descriptor used to send the
///                     message
#[derive(Copy, Clone, AsBytes, FromBytes, FromZeroes)]
#[repr(C)]
pub struct MsgHeader {
	pub msgtype: RawMsgType,
	pub whence: RawOffsetType,
	pub offset: RawOffset,
	pub client_write_size: usize,
	pub client_read_size: usize,
	pub client_id: i32,
	pub badge: u32,
}

impl MsgHeader {
	///Internal constructor that creates a blank header
	pub const fn new() -> MsgHeader {
		MsgHeader {
			msgtype: 0,
			whence: 0,
			offset: 0,
			client_write_size: 0,
			client_read_size: 0,
			client_id: 0,
			badge: 0,
		}
	}
	///Gets a direct reference to a `MsgHeader` from a `FixedIPCBuffer`
	///instance.
	pub fn from_buf(buf: &FixedIPCBuffer) -> &MsgHeader {
		MsgHeader::ref_from(&buf.header).expect("from_buf called on buffer of incorrect size (this should never happen!)")
	}
	///Gets a direct mutable reference to a `MsgHeader` from a
	///`FixedIPCBuffer` instance.
	pub fn from_buf_mut(buf: &mut FixedIPCBuffer) -> &mut MsgHeader {
		MsgHeader::mut_from(&mut buf.header).expect("from_buf_mut called on buffer of incorrect size (this should never happen!)")
	}
	///Gets a direct `MsgHeader` reference from the first buffer of a
	///`RecvLongMsgBuffer` array
	///
	///Returns None if there is no first buffer or the first buffer size
	///isn't the exact size of a header
	pub fn from_long_bufs<'a>(bufs: &'a mut [RecvLongMsgBuffer]) -> Option<&'a mut MsgHeader> {
		if bufs.len() == 0 {
			None
		}else{
			MsgHeader::mut_from(&mut bufs[0])
		}
	}

	///Internal method to copy the header to the first buffer of a
	///`RecvLongMsgBuffer` array
	///
	///Returns Err if there is no first buffer or the first buffer size
	///isn't the exact size of a header
	pub(crate) unsafe fn to_long_buf<'a>(&self, bufs: &'a mut [RecvLongMsgBuffer]) -> Result<(), ()> {
		if bufs.len() == 0 {
			Err(())
		}else{
			self.write_to(&mut bufs[0]).ok_or(())
		}

	}

	///Internal method to copy the header
	pub(crate) unsafe fn to_raw_buf(&self, ptr: *mut MsgHeader){
		debug_println!("MsgHeader::to_raw_buf {:p} {:p} {}", self, ptr, size_of::<Self>());
		copy_nonoverlapping(self, ptr, 1);
	}
	///Returns the message type as a `MsgType`
	pub fn msgtype_enum(&self) -> Option<MsgType> {
		MsgType::from_u64(self.msgtype as u64)
	}
	///Returns the offset type as an `OffsetType`
	pub fn whence_enum(&self) -> Option<OffsetType> {
		OffsetType::from_u64(self.whence as u64)
	}
}

impl Default for MsgHeader {
	#[inline]
	fn default() -> MsgHeader {
		MsgHeader::new()
	}
}

#[cfg(target_pointer_width = "64")]
pub(crate) const OFFSET_IDX: usize = 0;
#[cfg(target_pointer_width = "64")]
pub(crate) const SIZE_IDX: usize = 1;
#[cfg(target_pointer_width = "64")]
pub(crate) const RESERVED_REGS: usize = 2;

pub const CLIENT_ENDPOINT_COPIES: usize = 3;

///Bitfield struct for client message labels
#[cfg(target_pointer_width = "64")]
#[bitsize(64)]
#[derive(FromBits)]
pub struct ClientBadge {
    msgtype: u4,
    client_id: u30,
    user_badge: u30,
}

impl ClientBadge {
	pub fn unpack(value: usize) -> Result<(MsgType, i32, u32), (Offset, IOError)>{
		let badge = Self::from(value as u64);
		let raw_msgtype = badge.msgtype().value();
		let msgtype = if let Some(t) = MsgType::from_u8(raw_msgtype){
                        t
                }else{
                        return Err((SRV_ERR_NOSYS, IOError::InvalidMessage));
                };
		Ok((msgtype, badge.client_id().value() as i32, badge.user_badge().value()))
	}
	fn pack_single(msgtype: MsgType, client_id: i32, user_badge: u32) -> usize {
		#[cfg(target_pointer_width = "64")]
		return ClientBadge::new(u4::new(msgtype as u8), u30::new(client_id as u32), u30::new(user_badge)).value as usize;
	}
	pub fn pack(access: AccessMode, client_id: i32, user_badge: u32) -> [Option<usize>; CLIENT_ENDPOINT_COPIES]{
		match access {
			AccessMode::ReadOnly => [None, Some(Self::pack_single(MsgType::Read, client_id, user_badge)), None],
			AccessMode::WriteOnly => [Some(Self::pack_single(MsgType::Write, client_id, user_badge)), None, None],
			AccessMode::ReadWrite => [Some(Self::pack_single(MsgType::Write, client_id, user_badge)), Some(Self::pack_single(MsgType::Read, client_id, user_badge)), Some(Self::pack_single(MsgType::WriteRead, client_id, user_badge))],
		}
	}
}

///Bitfield struct for client message labels
#[cfg(target_pointer_width = "64")]
#[bitsize(64)]
#[derive(FromBits)]
pub(crate) struct ClientLabel {
    whence: u5,
    r_size: u10,
    w_remainder: u4,
    has_footer: bool,
    padding: u44,
}

impl ClientLabel {
	///Packs the parameters into a label.
	pub fn pack(whence: OffsetType, recv_short_size: usize, send_remainder: usize, has_footer: bool) -> usize {
		debug_println!("ClientLabel::pack: {} {} {} {}", whence as u8, recv_short_size as u16, send_remainder as u8, is_full);
		Self::new(
				u5::new(whence as u8),
				u10::new(recv_short_size as u16),
				u4::new(send_remainder as u8),
				has_footer,
		).value as usize
	}
	///Unpacks a label into separate parameters.
	pub fn unpack(value: usize) -> Result<(OffsetType, usize, usize, bool), (Offset, IOError)> {
		#[cfg(target_pointer_width = "64")]
		let label = Self::from(value as u64);
		let whence = if let Some(t) = OffsetType::from_u8(label.whence().into()){
			t
		}else{
			return Err((SRV_ERR_INVAL, IOError::InvalidMessage));
		};
		Ok((whence,
		label.r_size().value() as usize,
		label.w_remainder().value() as usize,
		label.has_footer()))
	}
}

///Bitfield struct for server message labels
#[cfg(target_pointer_width = "64")]
#[bitsize(64)]
#[derive(FromBits)]
pub(crate) struct ServerLabel {
	errno: u32,
	cw_size: u10,
	sw_size_remainder: u4,
	has_footer: bool,
	padding: u17,
}

impl ServerLabel {
	///Packs the parameters into a label.
	pub fn pack(errno: u32, cw_short_size: usize, w_remainder: usize, has_footer: bool) -> usize {
		Self::new(
				errno,
				u10::new(cw_short_size as u16),
				u4::new(w_remainder as u8),
				has_footer,
		).value as usize
	}
	///Unpacks a label into separate parameters.
	pub fn unpack(value: usize) -> (u32, usize, usize, bool) {
		#[cfg(target_pointer_width = "64")]
		let label = Self::from(value as u64);
		(label.errno(),
		label.cw_size().value() as usize,
		label.sw_size_remainder().value() as usize,
		label.has_footer())
	}
	///Generates a label for an error.
	pub fn pack_err(errno: u32) -> usize {
		Self::pack(errno + seL4_NumErrors, 0, 0, false)
	}
}

///Gets the primary fixed buffer.
#[inline]
pub(crate) fn get_primary_buffer() -> FixedIPCBuffer {
	let buf = unsafe { &mut*(seL4_GetIPCBuffer()) };
	let addr = &buf.msg as *const _ as usize;
	let data = unsafe { BufferArray::new(addr as *mut u8, (seL4_MsgMaxLength as usize - RESERVED_REGS) * size_of::<usize>()) };
	FixedIPCBuffer::new(BufferArray::null(), data)
}

///Gets the secondary fixed buffer (which allows interoperability between short
///and long IPC).
#[inline]
pub(crate) fn get_secondary_buffer() -> FixedIPCBuffer {
	let addr = unsafe { &SECONDARY_BUFFER as *const _ as usize };
	let data = unsafe { BufferArray::new(addr as *mut u8, SECONDARY_BUFFER.len()) };
	FixedIPCBuffer::new(BufferArray::null(), data)
}

///Gets the short message size in words and whether the message needs a footer
pub(crate) fn get_send_word_size(size: usize, offset: Offset, write_size: ServerStatus) -> Result<(usize, bool), IOError> {
	let mut send_size = size / size_of::<usize>();

	if size % size_of::<usize>() > 0 {
		send_size += 1;
	}

	if send_size > seL4_MsgMaxLength as usize - RESERVED_REGS {
		Err(IOError::MessageTooLong)
	}else{
		Ok((send_size, offset != 0 || write_size as usize > FIXED_BUF_MAX_SIZE))
	}
}

///A client-server communication channel implementing Unix-like file semantics
///on top of seL4 endpoints and/or notifications
///
///
///This is the internal part that is shared between the client and server code.
///This uses endpoints to implement what is basically unstructured RPC with two methods.
///
///On the client side, both readbuf() and writebuf() translate into seL4_Call.
///
///On the server side, readbuf() translates into seL4_Recv, and writebuf() into seL4_Send()
///

#[derive(Clone)]
pub(crate) struct BaseChannelFileDescriptor {
	base_fd: BaseFileDescriptor,
}


//TODO: add support for non-blocking mode (which can be done by simply switching blocking for non-blocking calls as necessary)
//TODO: add support for message-oriented files (this will only affect the traditional APIs, and will have the effect of making them preserve message boundaries

impl BaseChannelFileDescriptor {
	///Creates a new `BaseChannelFileDescriptor`
	pub fn new(access: AccessMode, client_write_endpoint: Option<Endpoint>, client_read_endpoint: Option<Endpoint>, client_writeread_endpoint: Option<Endpoint>, server_endpoint: Option<Endpoint>, flags: usize) -> BaseChannelFileDescriptor {
		BaseChannelFileDescriptor {
			base_fd: BaseFileDescriptor::new(
				client_write_endpoint.unwrap_or(Endpoint::from_cap(0)).to_cap(),
				client_read_endpoint.unwrap_or(Endpoint::from_cap(0)).to_cap(),
				client_writeread_endpoint.unwrap_or(Endpoint::from_cap(0)).to_cap(),
				server_endpoint.unwrap_or(Endpoint::from_cap(0)).to_cap(),
				0, 0, access, flags),
		}
	}
	///Common implementation of getbuf_r() for channels
	#[inline]
	pub(crate) fn getbuf_r(&self) -> Option<FixedIPCBuffer> {
		debug_println!("getbuf_r: {:?}", READ_BUF_TYPE.get());
		match READ_BUF_TYPE.get() {
			ReadBufType::Unset => None,
			ReadBufType::Primary => Some(get_primary_buffer()),
			ReadBufType::Secondary => Some(get_secondary_buffer()),
		}
	}
	///Common implementation of getbuf_w() for channels
	#[inline]
	pub(crate) fn getbuf_w(&self) -> Option<FixedIPCBuffer> {
		Some(get_primary_buffer())
	}
	///Internal method to set the type of buffer returned by getbuf_r().
	pub(crate) fn set_read_type(&self, read_type: ReadBufType) {
		READ_BUF_TYPE.set(read_type);
	}
	///Gets the access mode
	#[inline]
	pub fn get_access(&self) -> AccessMode {
		self.base_fd.get_access()
	}
	///Gets the client endpoint for writes associated with this FD (if there
	///is one)
	#[inline]
	pub fn get_client_write_endpoint(&self) -> Endpoint {
		self.base_fd.get_client_write_endpoint()
	}
	///Gets the client endpoint for reads associated with this FD (if there
	///is one)
	#[inline]
	pub fn get_client_read_endpoint(&self) -> Endpoint {
		self.base_fd.get_client_read_endpoint()
	}
	///Gets the client endpoint for combined write/reads associated with
	///this FD (if there is one)
	#[inline]
	pub fn get_client_writeread_endpoint(&self) -> Endpoint {
		self.base_fd.get_client_writeread_endpoint()
	}
	///Gets the client endpoint associated with this FD (if there is one)
	#[inline]
	pub fn get_server_endpoint(&self) -> Endpoint {
		self.base_fd.get_server_endpoint()
	}
	#[inline]
	pub fn getctl(&self) -> i32 {
		self.base_fd.getctl()
	}
	///Internal method called after a syscall to copy between fixed buffers
	///and set the buffer type
	pub fn handle_fixed_read_bufs(&self, short_size: usize, long_size: usize) -> usize {
		if long_size > 0 {
			if short_size > 0 {
				let primary = get_primary_buffer();
				let secondary = get_secondary_buffer();
				secondary.get_data()[0..short_size].copy_from_slice(&primary.get_data()[0..short_size]);
			}
			self.set_read_type(ReadBufType::Secondary);
			long_size
		}else{
			self.set_read_type(ReadBufType::Primary);
			short_size
		}
	}
	///Internal wrapper to get the long IPC flags for syscalls
	#[inline]
	pub fn get_flags(&self) -> u64 {
		self.base_fd.get_flags()
	}
	///Internal wrapper to set the long IPC flags for syscalls
	#[allow(unused)]
	#[inline]
	pub fn set_flags(&self, flags: u64) {
		self.base_fd.set_flags(flags);
	}
}
