// Copyright 2022-2024 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! This provides a Unix-like read/write/seek API directly on top of kernel
//! objects (without requiring any intermediary servers). Several extended
//! variants of functions to deal with functionality not present in conventional
//! Unix are provided.
//!
//! The APIs provided consist of reads that transfer data into a buffer,
//! writes that transfer data out of a buffer
//!
//! Four basic types of file descriptors are supported. The API is mostly
//! orthogonal across all of them, and most functions work on all file
//! descriptor types, although parameters not relevant to a particular type must
//! be set to a dummy value (in all cases there are also function variants that
//! don't require the irrelevant parameters).
//!
//! Client channels have basically the same semantics as conventional Unix file
//! descriptors. These act as unstructured RPC channels that send requests to
//! servers, blocking until a reply is received.
//!
//! Server channels accept requests from client channels. A header with metadata
//! such as the message type and buffer sizes is prepended to the message when
//! reading from a server channel. These are basically read-only; replying is
//! instead done through server message descriptors. Special read APIs that
//! store the rights to the received message in a user-specified server message
//! descriptor are provided.
//!
//! Server message descriptors access individual client messages. They are used
//! to send replies and also provide for unlimited access to the client-side
//! buffers of a message. These start out in an empty state and cannot be used
//! until a message has been received into them. Replying to a message returns
//! the message descriptor to the empty state, and it may then be used to
//! receive another message.
//!
//! Notification file descriptors provide a non-blocking signalling mechanism,
//! functioning as a collection of binary semaphores. Reading them waits for a
//! signal, and writing them signals them without blocking.
//!
//! The APIs provided comprise reads that transfer data into a buffer, writes
//! that transfer data out of a buffer, combined write/reads that function as
//! atomic combinations of a write followed by a read, and a few utility
//! functions (seek is the only one of these that actually sends a message,
//! although it is implemented as a zero-length read rather than a distinct
//! primitive). Unlike most other IPC transport layers for microkernels, no
//! support for other types of messages or structured data is implemented (this
//! is left up to higher-level layers instead), nor are raw microkernel syscalls
//! exposed, since the transport layer is just a thin wrapper around them.
//!
//! In addition to the usual APIs that accept buffers with arbitrary addresses
//! and sizes, APIs that use a thread-local fixed buffer are also provided. Both
//! types of APIs are interoperable (clients and servers don't have to care what
//! type of buffer the other end uses), although there may be some overhead when
//! mixing buffer types. Fixed-buffer APIs may use a fast path in the kernel
//! that transfers messages in registers instead of copying if both sides are
//! using fixed buffers and the message is small enough.
//!

#![no_std]

#![feature(thread_local)]

#[allow(unused_imports)]
#[macro_use]
extern crate log;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate enum_primitive_derive;
#[macro_use]
extern crate zerocopy;
#[macro_use]
extern crate zerocopy_derive;

extern crate num_traits;
extern crate intrusive_collections;
extern crate static_assertions;
extern crate bilge;

extern crate sel4;
extern crate sel4_sys;

mod channel;
mod client;
mod notification;
mod server;

pub use crate::channel::{
	CLIENT_ENDPOINT_COPIES,
	ClientBadge,
	FIXED_BUF_MAX_SIZE,
	HEADER_SIZE,
	MsgHeader,
	MsgType,
};
pub use crate::server::{
	ServerChannelFileDescriptor,
	ServerMessageDescriptor,
	ServerChannelOptions,
	setmd
};
pub use crate::notification::NOTIFICATION_MSG_SIZE;

pub use crate::client::{
	ClientFileDescriptor,
	shared_client_clunk,
};

pub use notification::NotificationFileDescriptor;

use core::slice;
use core::ops::{Deref, DerefMut, Index, IndexMut};
use core::mem::size_of;
use core::marker::PhantomData;
use core::sync::atomic::{
	AtomicI64,
	AtomicU64,
	Ordering,
};

use sel4::{
	Endpoint,
	ErrorDetails,
	FromCap,
	Notification,
	PAGE_SIZE,
	Reply,
	seL4_CPtr,
};

pub use sel4::{
	RecvLongMsgBuffer,
	SendLongMsgBuffer,
};

use intrusive_collections::UnsafeRef;

use static_assertions::const_assert;

#[macro_export]
macro_rules! debug_println {
	($($toks:tt)*) => ({
		#[cfg(feature = "debug_msgs")]
		info!($($toks)*);
	})
}

pub type Offset = i64;
pub type ServerStatus = i64;
pub type RawFileDescriptor = i32;
pub type MsgSize = u32;

///The permissions (access mode) of a file descriptor
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum AccessMode {
	ReadOnly,
	WriteOnly,
	ReadWrite,
}

impl AccessMode {
	///Returns true iff `other` is a subset of this access mode
	pub fn is_subset(&self, other: AccessMode) -> bool {
		match (self, other) {
			(Self::ReadWrite, Self::ReadOnly) => true,
			(Self::ReadOnly, Self::ReadOnly) => true,
			(Self::ReadWrite, Self::WriteOnly) => true,
			(Self::WriteOnly, Self::WriteOnly) => true,
			(Self::ReadWrite, Self::ReadWrite) => true,
			_ => false,
		}
	}
}

//TODO: add a distinct Poll message type for client-side polling (the server will reply to this with the readable/writable/exceptional status in the buffer instead of data); server-side polling will instead first check if there is a pending reply and poll as writable if so, otherwise it will just do a regular receive and poll as readable when a message is received

//TODO: add polling support using a helper thread for each thread FD and a wakeup notifcation for each thread (which could probably just be the regular park notification for process server threads; user threads will need a notification allocated specifically for this since they won't have a park notification tracked by the process server); if trees or linked lists are used, the memory will have to be caller-provided

///Offset type code
///
///Which offsets and types are accepted is server-dependent. Some servers only
///accept zero offsets for instance (such as those with RPC/pipe-like APIs).
///
///Start specifies and offset relative to the beginning of the file
///Current specifies an offset relative to the current offset
///End specifies an offset relative to the current end of the file for client
///     channel FDs. Writing to a server message descriptor with this offset
///     type replies to the message, after which the message descriptor cannot
///     be used until another message has been accepted on it. The offset
///     value in this case is treated as an absolute offset. This is not
///     accepted for reads from server message descriptors.
///Hole specifies the offset of the next hole greater than or equal to the
///    	provided offset (relative to the beginning; if the provided offset is
///    	within a hole this is equivalent to Start).
///Data specifies the offset of the next data region greater than or equal to
///    	the provided offset (relative to the beginning; if the provided offset
///    	is within a data region this is equivalent to Start).
///Poll specifies that the server should return once data is available,
///     ignoring buffers and not updating the stored offset. This is used for
///     polling. The offset is interpreted as a set of flags for events in which
///     the client is interested.
///All of these also have Update* variants that update the server-side offset
///when used on client channels.
#[derive(Clone, Copy, Debug, PartialEq, Primitive)]
pub enum OffsetType {
	Start = 0,
	Current = 1,
	End = 2,
	Hole = 3,
	Data = 4,
	Poll = 5,
	UpdateStart = 0x10,
	UpdateCurrent = 0x11,
	UpdateEnd = 0x12,
	UpdateHole = 0x13,
	UpdateData = 0x14,
}

impl OffsetType {
	///Gets the updating variant of this offset type
	pub fn update(self) -> OffsetType {
		match self {
			Self::Start => Self::UpdateStart,
			Self::Current => Self::UpdateCurrent,
			Self::End => Self::UpdateEnd,
			Self::Hole => Self::UpdateHole,
			Self::Data => Self::UpdateData,
			_ => self,
		}
	}
	///Gets the non-updating variant of this offset type
	pub fn base(self) -> OffsetType {
		match self {
			Self::UpdateStart => Self::Start,
			Self::UpdateCurrent => Self::Current,
			Self::UpdateEnd => Self::End,
			Self::UpdateHole => Self::Hole,
			Self::UpdateData => Self::Data,
			_ => self,
		}
	}
	pub fn is_update(self) -> bool {
		self as usize & OFFSET_UPDATE as usize != 0
	}
}

pub const OFFSET_UPDATE: OffsetType = OffsetType::UpdateStart;

pub const POLLIN: Offset = 0x001;
pub const POLLPRI: Offset = 0x002;
pub const POLLOUT: Offset = 0x004;

static mut NULL_BUFFER: [u8; 0] = [];

///TODO: specify a lifetime for buffers
///
///Fixed-length IPC buffers associated with a file descriptor
///
///The status buffer has a non-zero length only for server-side FDs.
#[derive(Clone, Copy, Debug)]
pub struct FixedIPCBuffer {
	header: BufferArray,
	data: BufferArray,
}

impl FixedIPCBuffer {
	///Internal function to create a new instance
	pub(crate) fn new(header: BufferArray, data: BufferArray) -> FixedIPCBuffer {
		FixedIPCBuffer {
			header,
			data,
		}
	}
	///Gets the maximum message payload size (excluding the status buffer)
	#[inline]
	pub fn get_max_message_size(&self) -> usize {
		self.data.len()
	}
	///Gets the status buffer
	#[inline]
	pub fn get_header(&self) -> BufferArray {
		self.header
	}
	///Gets the primary data buffer
	#[inline]
	pub fn get_data(&self) -> BufferArray {
		self.data
	}
}

///Internal function to get a send long vector from a fixed buffer
#[inline]
fn get_long_send_buf(data: &BufferArray, size: usize) -> Result<[SendLongMsgBuffer; 1], IOError> {
	if size > data.len(){
		Err(IOError::InvalidArgument)
	}else{
		Ok([SendLongMsgBuffer::new(&data[0..size])])
	}
}

///Internal function to get a receive long vector from a fixed buffer
#[inline]
fn get_long_recv_buf(data: &mut BufferArray, size: usize) -> Result<[RecvLongMsgBuffer; 1], IOError> {
	if size > data.len(){
		Err(IOError::InvalidArgument)
	}else{
		Ok([RecvLongMsgBuffer::new(&mut data[0..size])])
	}
}


///Errors returned by transport layer functions
#[derive(Clone, Copy, Debug, PartialEq, Fail)]
pub enum IOError {
	#[fail(display = "Resource temporarily unavailable")]
	WouldBlock,
	#[fail(display = "Invalid argument")]
	InvalidArgument,
	#[fail(display = "Message too long")]
	MessageTooLong,
	#[fail(display = "Invalid message")]
	InvalidMessage,
	#[fail(display = "Server error")]
	ServerError(ServerStatus),
	#[fail(display = "Invalid operation")]
	InvalidOperation,
	#[fail(display = "System call error")]
	SyscallError(sel4::Error),
}

impl IOError {
	///Returns true if the error should be considered as a clunk rather than
	///a true error.
	fn is_clunk(&self) -> bool {
		if let IOError::SyscallError(err) = self {
			is_clunk_err(err)
		}else{
			false
		}
	}
	///Returns the errno corresponding to this `IOError`
	pub fn to_errno(&self) -> ServerStatus {
		match self {
			Self::WouldBlock => SRV_ERR_AGAIN,
			Self::InvalidArgument => SRV_ERR_INVAL,
			Self::MessageTooLong => SRV_ERR_MSGSIZE,
			Self::InvalidMessage => SRV_ERR_PROTO,
			Self::ServerError(err) => *err,
			Self::InvalidOperation => SRV_ERR_BADF,
			Self::SyscallError(err) => {
				match err.details() {
					Some(ErrorDetails::ThreadCancelled) => SRV_ERR_SRCH,
					Some(ErrorDetails::InvalidLocalIPCAddress) => SRV_ERR_FAULT,
					Some(ErrorDetails::NotEnoughMemory { .. }) => SRV_ERR_NOMEM,
					Some(ErrorDetails::ReplySequenceError) => SRV_ERR_PROTO,
					_ => SRV_ERR_IO,
				}
			},
		}
	}
}

impl From<IOError> for ServerStatus {
	fn from(value: IOError) -> ServerStatus {
		value.to_errno()
	}
}

impl From<ServerStatus> for IOError {
	fn from(value: ServerStatus) -> IOError {
		IOError::ServerError(value)
	}
}

///Returns true if the syscall error should be considered as a clunk.
pub(crate) fn is_clunk_err(err: &sel4::Error) -> bool{
	match err.details() {
		Some(ErrorDetails::EndpointCancelled) => true,
		Some(ErrorDetails::InvalidCapability { which: _ }) => true,
		_ => false,
	}
}

//these are the same as the corresponding Linux errno values
//TODO: take these from errno.h once one is present
pub const SRV_ERR_PERM: ServerStatus = 1;
pub const SRV_ERR_NOENT: ServerStatus = 2;
pub const SRV_ERR_SRCH: ServerStatus = 3;
pub const SRV_ERR_IO: ServerStatus = 5;
pub const SRV_ERR_2BIG: ServerStatus = 7;
pub const SRV_ERR_BADF: ServerStatus = 9;
pub const SRV_ERR_AGAIN: ServerStatus = 11;
pub const SRV_ERR_NOMEM: ServerStatus = 12;
pub const SRV_ERR_FAULT: ServerStatus = 14;
pub const SRV_ERR_NOTDIR: ServerStatus = 20;
pub const SRV_ERR_ISDIR: ServerStatus = 21;
pub const SRV_ERR_INVAL: ServerStatus = 22;
pub const SRV_ERR_MFILE: ServerStatus = 24;
pub const SRV_ERR_NOSPC: ServerStatus = 28;
pub const SRV_ERR_NAMETOOLONG: ServerStatus = 36;
pub const SRV_ERR_NOSYS: ServerStatus = 38;
pub const SRV_ERR_LOOP: ServerStatus = 40;
pub const SRV_ERR_PROTO: ServerStatus = 71;
pub const SRV_ERR_MSGSIZE: ServerStatus = 90;
pub const SRV_ERR_NOTCONN: ServerStatus = 107;
///An individual message buffer
#[derive(Clone, Copy, Debug)]
pub struct BufferArray {
	ptr: *mut u8,
	len: usize,
}

impl BufferArray {
	///Creates a new `BufferArray`
	///
	///This is unsafe because the pointer isn't checked at all.
	pub unsafe fn new(ptr: *mut u8, len: usize) -> BufferArray {
		BufferArray {
			ptr,
			len,
		}
	}
	///Creates a null `BufferArray`
	pub fn null() -> BufferArray {
		unsafe { Self::new(NULL_BUFFER.as_mut_ptr(), 0) }
	}
	///Returns a view of this array that contains `usize` instead of `u8`
	pub fn as_word(&self) -> WordBufferArray {
		unsafe { WordBufferArray::new(self.ptr as *mut usize, self.len) }
	}
}

impl Deref for BufferArray {
	type Target = [u8];

	fn deref(&self) -> &[u8] {
		unsafe { slice::from_raw_parts(self.ptr, self.len) }
	}
}

impl DerefMut for BufferArray {
	fn deref_mut(&mut self) -> &mut [u8] {
		unsafe { slice::from_raw_parts_mut(self.ptr, self.len) }
	}
}

///A `usize` view of a `BufferArray`
pub struct WordBufferArray {
	ptr: *mut usize,
	len: usize,
}

impl WordBufferArray {
	///Creates a new `WordBufferArray`
	#[inline]
	pub unsafe fn new(ptr: *mut usize, len: usize) -> WordBufferArray {
		WordBufferArray {
			ptr,
			len,
		}
	}
}

impl Deref for WordBufferArray {
	type Target = [usize];

	fn deref(&self) -> &[usize] {
		unsafe { slice::from_raw_parts(self.ptr, self.len) }
	}
}

impl DerefMut for WordBufferArray {
	fn deref_mut(&mut self) -> &mut [usize] {
		unsafe { slice::from_raw_parts_mut(self.ptr, self.len) }
	}
}

//TODO: it might be a good idea to use std::ptr::read_volatile() to read the contents, even though it isn't changed while an FD is open, just to make sure that the compiler doesn't make assumptions about how it is initialized
///A Vec-like array backed by a pre-allocated region
pub struct UnsafeArray<T: Send + Sync> {
	size: AtomicU64,
	ptr: AtomicU64,
	align: u64,
	default_addr: u64,
	phantom: PhantomData<T>,
}

impl<T: Send + Sync> UnsafeArray<T> {
	///Creates a new `UnsafeArray`
	pub unsafe fn new(len: usize, ptr: usize, align: usize, default_addr: usize) -> UnsafeArray<T> {
		UnsafeArray {
			size: AtomicU64::new(len as u64),
			ptr: AtomicU64::new(ptr as u64),
			align: align as u64,
			default_addr: default_addr as u64,
			phantom: Default::default(),
		}
	}
	///Returns the current length of this array
	pub fn len(&self) -> usize {
		self.size.load(Ordering::Relaxed) as usize
	}
	///Sets the current length of this array when the underlying region
	///has been changed
	pub unsafe fn set_len(&self, len: usize) {
		self.size.store(len as u64, Ordering::Relaxed)
	}
	///Sets the start pointer of this array when it has been moved
	pub unsafe fn set_ptr(&self, ptr: usize) {
		self.ptr.store(ptr as u64, Ordering::Relaxed)
	}
	fn get_ptr(&self) -> usize {
		self.ptr.load(Ordering::Relaxed) as usize
	}
}

impl<T: Send + Sync> Index<usize> for UnsafeArray<T> {
	type Output = T;
	fn index(&self, index: usize) -> &Self::Output {
		let ptr = self.get_ptr();
		let addr = if index < self.len() && ptr != 0{
			ptr + self.align as usize * index
		}else{
			self.default_addr as usize
		};
		unsafe { &*(addr as *const T) }
	}
}

impl<T: Send + Sync> IndexMut<usize> for UnsafeArray<T> {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		if index > self.len() {
			panic!("UnsafeArray index {} out of range", index);
		}
		let addr = self.get_ptr() + self.align as usize * index;
		unsafe { &mut *(addr as *mut T) }
	}
}

impl<T: Send + Sync> Clone for UnsafeArray<T> {
	fn clone(&self) -> Self {
		UnsafeArray {
			size: AtomicU64::new(self.size.load(Ordering::Relaxed)),
			ptr: AtomicU64::new(self.ptr.load(Ordering::Relaxed)),
			align: self.align,
			default_addr: self.default_addr,
			phantom: Default::default(),
		}
	}
}

macro_rules! wrapper_impls {
	($fd_struct: ty) => {
		impl FileDescriptor for $fd_struct {
			fn get_reply(&self) -> Option<Reply> {
				self.get_base_fd().get_reply()
			}
			fn get_access(&self) -> AccessMode {
				self.get_base_fd().get_access()
			}
			fn getbuf_r(&self) -> Option<FixedIPCBuffer> {
				self.get_base_fd().getbuf_r()
			}
			fn getbuf_w(&self) -> Option<FixedIPCBuffer> {
				self.get_base_fd().getbuf_w()
			}
			fn seterrno(&self, value: Offset) {
				self.get_base_fd().seterrno(value);
			}
			fn wpread(&self, buf: &mut [u8], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().wpread(buf, offset, whence)
			}
			fn pread(&self, buf: &mut [u8], offset: Offset) -> Result<usize, IOError> {
				self.get_base_fd().pread(buf, offset)
			}
			fn wpreadv(&self, bufs: &mut [RecvLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().wpreadv(bufs, offset, whence)
			}
			fn preadv(&self, bufs: &mut [RecvLongMsgBuffer], offset: Offset) -> Result<usize, IOError> {
				self.get_base_fd().preadv(bufs, offset)
			}
			fn read(&self, buf: &mut [u8]) -> Result<usize, IOError> {
				self.get_base_fd().read(buf)
			}
			fn readv(&self, bufs: &mut [RecvLongMsgBuffer]) -> Result<usize, IOError> {
				self.get_base_fd().readv(bufs)
			}
			fn mread(&self, buf: &mut [u8], md: RawFileDescriptor) -> Result<usize, IOError> {
				self.get_base_fd().mread(buf, md)
			}
			fn mreadv(&self, bufs: &mut [RecvLongMsgBuffer], md: RawFileDescriptor) -> Result<usize, IOError> {
				self.get_base_fd().mreadv(bufs, md)
			}
			fn wpreadb(&self, size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().wpreadb(size, offset, whence)
			}
			fn preadb(&self, size: usize, offset: Offset) -> Result<usize, IOError> {
				self.get_base_fd().preadb(size, offset)
			}
			fn readb(&self, size: usize) -> Result<usize, IOError> {
				self.get_base_fd().readb(size)
			}
			fn mreadb(&self, size: usize, md: RawFileDescriptor) -> Result<usize, IOError> {
				self.get_base_fd().mreadb(size, md)
			}
			fn seek(&self, offset: Offset, whence: OffsetType) -> Result<Offset, IOError> {
				self.get_base_fd().seek(offset, whence)
			}
			fn wpwrite(&self, buf: &[u8], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().wpwrite(buf, offset, whence)
			}
			fn pwrite(&self, buf: &[u8], offset: Offset) -> Result<usize, IOError> {
				self.get_base_fd().pwrite(buf, offset)
			}
			fn wpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().wpwritev(bufs, offset, whence)
			}
			fn pwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset) -> Result<usize, IOError> {
				self.get_base_fd().pwritev(bufs, offset)
			}
			fn mwpwrite(&self, buf: &[u8], offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().mwpwrite(buf, offset, whence, status)
			}
			fn mwpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().mwpwritev(bufs, offset, whence, status)
			}
			fn mwpwriteb(&self, len: usize, offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().mwpwriteb(len, offset, whence, status)
			}
			fn write(&self, buf: &[u8]) -> Result<usize, IOError> {
				self.get_base_fd().write(buf)
			}
			fn writev(&self, bufs: &[SendLongMsgBuffer]) -> Result<usize, IOError> {
				self.get_base_fd().writev(bufs)
			}
			fn wpwriteb(&self, size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
				self.get_base_fd().wpwriteb(size, offset, whence)
			}
			fn pwriteb(&self, size: usize, offset: Offset) -> Result<usize, IOError> {
				self.get_base_fd().pwriteb(size, offset)
			}
			fn writeb(&self, size: usize) -> Result<usize, IOError> {
				self.get_base_fd().writeb(size)
			}
			fn mpwriteread(&self, w_buf: &[u8], w_offset: Offset, r_buf: &mut [u8], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
				self.get_base_fd().mpwriteread(w_buf, w_offset, r_buf, status, md)
			}
			fn mpwritereadv(&self, w_buf: &[SendLongMsgBuffer], w_offset: Offset, r_buf: &mut [RecvLongMsgBuffer], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
				self.get_base_fd().mpwritereadv(w_buf, w_offset, r_buf, status, md)
			}
			fn mpwritereadb(&self, w_len: usize, w_offset: Offset, r_len: usize, status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
				self.get_base_fd().mpwritereadb(w_len, w_offset, r_len, status, md)
			}
			fn writeread(&self, w_buf: &[u8], r_buf: &mut [u8]) -> Result<(usize, usize), IOError> {
				self.get_base_fd().writeread(w_buf, r_buf)
			}
			fn writereadv(&self, w_buf: &[SendLongMsgBuffer], r_buf: &mut [RecvLongMsgBuffer]) -> Result<(usize, usize), IOError> {
				self.get_base_fd().writereadv(w_buf, r_buf)
			}
			fn mwriteread(&self, w_buf: &[u8], r_buf: &mut [u8], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
				self.get_base_fd().mwriteread(w_buf, r_buf, status, md)
			}
			fn mwritereadv(&self, w_bufs: &[SendLongMsgBuffer], r_bufs: &mut [RecvLongMsgBuffer], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
				self.get_base_fd().mwritereadv(w_bufs, r_bufs, status, md)
			}
			fn writereadb(&self, w_len: usize, r_len: usize) -> Result<(usize, usize), IOError> {
				self.get_base_fd().writereadb(w_len, r_len)
			}
			fn mwritereadb(&self, w_len: usize, r_len: usize, status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
				self.get_base_fd().mwritereadb(w_len, r_len, status, md)
			}
			fn getctl(&self) -> Result<RawFileDescriptor, IOError>{
				Err(IOError::InvalidOperation)
			}
			fn setctl(&self, _fd: RawFileDescriptor) -> Result<(), IOError>{
				Err(IOError::InvalidOperation)
			}
		}
	}
}

///Internal enum to specify whether to only look up message descriptors (used by
///in m*read methods of server channels)
#[derive(Copy, Clone, Debug)]
enum AllowedFDType {
	Any,
	MessageDescriptor,
}

///A reference wrapper to a file descriptor in the calling thread's FDSpace
///
///This looks up the underlying FD by index on every call
#[derive(Copy, Clone, Debug)]
pub struct FileDescriptorRef {
	index: RawFileDescriptor,
	allowed_type: AllowedFDType,
}

impl FileDescriptorRef {
	///Internal method to get the inner file descriptor and allowed type for
	///matching
	fn get_inner(&self) -> (&UnifiedFileDescriptor, AllowedFDType) {
		debug_println!("FileDescriptorRef::get_inner: {}", self.index);
		let inner = if self.index < 0 {
			debug_println!("using reserved");
			unsafe { RESERVED_FDS.as_ref().expect("thread FDSpace unset").get(self.index) }
		}else{
			debug_println!("using main");
			unsafe { FDS.as_ref().expect("thread FDSpace unset").get(self.index) }
		};
		(inner, self.allowed_type)
	}
	///Internal method to get the inner file descriptor if it matches the
	///allowed type
	fn get_base_fd(&self) -> &dyn FileDescriptor {
		debug_println!("FileDescriptorRef::get_base_fd: {} {:?}", self.index, self.allowed_type);
		match self.get_inner() {
			(UnifiedFileDescriptor::ChannelClient(ref fd), AllowedFDType::Any) => {
				debug_println!("ChannelClient");
				fd
			},
			(UnifiedFileDescriptor::ChannelServer(ref fd), AllowedFDType::Any) => {
				debug_println!("ChannelServer");
				fd
			},
			(UnifiedFileDescriptor::ChannelServerMessage(ref fd), _) => {
				debug_println!("ChannelServerMessage");
				fd
			},
			(UnifiedFileDescriptor::Notification(ref fd), AllowedFDType::Any) => {
				debug_println!("Notification");
				fd
			},
			(_, _) => {
				debug_println!("invalid");
				&NULL_FD
			},
		}

	}
	///Gets the underlying index of this `FileDescriptorRef`
	pub fn get_id(&self) -> RawFileDescriptor {
		self.index
	}
}

wrapper_impls!(FileDescriptorRef);


struct NullFileDescriptor {
}

impl FileDescriptor for NullFileDescriptor {
}

pub const FD_ALIGN: usize = 1024;
const_assert!(size_of::<UnifiedFileDescriptor>() < FD_ALIGN);
const_assert!(PAGE_SIZE % FD_ALIGN == 0);

const NULL_FD: NullFileDescriptor = NullFileDescriptor {
};
const NULL_INNER_FD: UnifiedFileDescriptor = UnifiedFileDescriptor::Null;

///An array of file descriptors for a thread
pub struct FDArray {
	fds: UnsafeArray<UnifiedFileDescriptor>,
}

impl FDArray {
	///Creates a new `FDArray`
	pub unsafe fn new(len: usize, ptr: usize) -> FDArray {
		FDArray {
			fds: UnsafeArray::new(len, ptr, FD_ALIGN, &NULL_INNER_FD as *const UnifiedFileDescriptor as usize),
		}
	}
	///Panics if the structure layout is invalid
	pub fn check_struct_layout(&self){
		if self.len() > 0 {
			match self.get(0) {
				UnifiedFileDescriptor::Null => {},
				_ => panic!("FDArray: Rust structure layout changed, or array was initialized with non-zero memory"),
			}
		}
	}
	///Gets a file descriptor
	pub fn get(&self, index: RawFileDescriptor) -> &UnifiedFileDescriptor {
		if index >= 0 && (index as usize) < self.fds.len() {
			debug_println!("FDArray: {:x} {} fd {} found", self.fds.ptr.load(Ordering::Relaxed), self.fds.len(), index);
			&self.fds[index as usize]
		}else{
			debug_println!("FDArray: {:x} fd {} not found", self.fds.ptr.load(Ordering::Relaxed), index);
			&UnifiedFileDescriptor::Null
		}
	}
	///Inserts a file descriptor
	pub fn insert(&mut self, index: RawFileDescriptor, fd: UnifiedFileDescriptor) {
		debug_println!("FDArray: {:x} insert at {}", self.fds.ptr.load(Ordering::Relaxed), index);
		if index >= 0 {
			self.fds[index as usize] = fd;
		}
	}
	///Removes a file descriptor
	pub fn remove(&mut self, index: RawFileDescriptor){
		debug_println!("FDArray: {:x} remove {}", self.fds.ptr.load(Ordering::Relaxed), index);
		self.fds[index as usize] = UnifiedFileDescriptor::Null;
	}
	///Updates the length of the array when the underlying region has been
	///changed
	pub unsafe fn set_len(&self, len: usize) {
		self.fds.set_len(len);
	}
	///Sets the start pointer of the array when the underlying region has
	///been changed
	pub unsafe fn set_ptr(&self, ptr: usize) {
		self.fds.set_ptr(ptr);
	}
	///Gets the length of the array
	pub fn len(&self) -> usize {
		self.fds.len()
	}
}

impl Clone for FDArray {
	fn clone(&self) -> FDArray {
		FDArray {
			fds: self.fds.clone(),
		}
	}
}

///Converts from a negative index to a positive index in the reserved array
pub fn get_reserved_index(index: RawFileDescriptor) -> RawFileDescriptor {
	(index + 1).abs()
}

///Holds thread-local reseved file descriptors
pub struct ReservedFDArray {
	contents: UnsafeRef<FDArray>,
}
impl ReservedFDArray {
	///Create a new `ReservedFDArray`
	pub fn new(contents: UnsafeRef<FDArray>) -> Self {
		Self {
			contents,
		}
	}
	///Gets an FD from the array. Indices are negative and start at -1.
	pub fn get(&self, mut index: RawFileDescriptor) -> &UnifiedFileDescriptor {
		debug_println!("ReservedFDArray::get: {}", index);
		index = get_reserved_index(index);
		if (index as usize) < self.contents.len() {
			debug_println!("ReservedFDArray: found");
			&self.contents.get(index)
		}else{
			debug_println!("ReservedFDArray: not found");
			&UnifiedFileDescriptor::Null
		}
	}
}

#[thread_local]
static mut FDS: Option<UnsafeRef<FDArray>> = None;
#[thread_local]
static mut RESERVED_FDS: Option<ReservedFDArray> = None;


///Sets the calling thread's `FDArray`
pub fn set_fd_arrays(main_array: UnsafeRef<FDArray>, reserved_array: UnsafeRef<FDArray>){
	unsafe { FDS = Some(main_array) };
	unsafe { RESERVED_FDS = Some(ReservedFDArray::new(reserved_array)) };
}

///Gets a `FileDescriptorRef` from this thread's `FDArray`
pub fn get_fd(index: RawFileDescriptor) -> FileDescriptorRef {
	debug_println!("get_fd: {}", index);
	FileDescriptorRef {
		index,
		allowed_type: AllowedFDType::Any
	}
}

///Gets a `FileDescriptorRef` from this thread's `FDArray` that is only valid if
///the slot has a message descriptor in it
pub fn get_md(index: RawFileDescriptor) -> FileDescriptorRef {
	debug_println!("get_fd: {}", index);
	FileDescriptorRef {
		index,
		allowed_type: AllowedFDType::MessageDescriptor,
	}
}

#[cfg(target_pointer_width = "64")]
type RawMsgType = u32;
#[cfg(target_pointer_width = "64")]
type RawOffsetType = u32;

//TODO: add a non-blocking flag

///Converts a result with an offset into one without
#[inline]
fn convert_from_offset_result(result: Result<(usize, Offset), IOError>) -> Result<usize, IOError>{
	match result {
		Ok((size, _)) => Ok(size),
		Err(err) => Err(err),
	}
}

///Converts a result without an offset into one with
#[inline]
fn convert_to_offset_result(result: Result<usize, IOError>) -> Result<(usize, Offset), IOError>{
	match result {
		Ok(size) => Ok((size, 0)),
		Err(err) => Err(err),
	}
}

///Common inner structure for all file descriptors
///
///All file descriptor types should be repr(C) and contain a single field of
///this type, in order to keep the layout of all variants the same. This limits
///brokenness when an FD gets closed by a different thread sharing the same
///FDSpace; separate fields for each CPtr means that a CPtr of one type will
///never get misinterpreted by the transport layer as another type even if the
///FD structure got replaced underneath it with a different type, nor will any
///other fields get misinterpreted as something else entirely
#[derive(Debug)]
#[repr(C)]
pub(crate) struct BaseFileDescriptor {
	client_write_endpoint: seL4_CPtr,
	client_read_endpoint: seL4_CPtr,
	client_writeread_endpoint: seL4_CPtr,
	server_endpoint: seL4_CPtr,
	reply: seL4_CPtr,
	notification: seL4_CPtr,
	access: AccessMode,
	control_fd: RawFileDescriptor,
	flags: AtomicU64,
	errno: AtomicI64,
}

impl BaseFileDescriptor {
	///Creates a new `BaseFileDescriptor`
	pub fn new(client_write_endpoint: seL4_CPtr, client_read_endpoint: seL4_CPtr, client_writeread_endpoint: seL4_CPtr, server_endpoint: seL4_CPtr, reply: seL4_CPtr, notification: seL4_CPtr, access: AccessMode, flags: usize) -> BaseFileDescriptor {
		BaseFileDescriptor {
			client_write_endpoint,
			client_read_endpoint,
			client_writeread_endpoint,
			server_endpoint,
			reply,
			notification,
			access,
			control_fd: -1,
			flags: AtomicU64::new(flags as u64),
			errno: AtomicI64::new(0),
		}
	}
	///Gets the endpoint for sending writes on the client side
	#[inline]
	pub fn get_client_write_endpoint(&self) -> Endpoint {
		Endpoint::from_cap(self.client_write_endpoint)
	}
	///Gets the endpoint for sending reads on the client side
	#[inline]
	pub fn get_client_read_endpoint(&self) -> Endpoint {
		Endpoint::from_cap(self.client_read_endpoint)
	}
	///Gets the endpoint for sending combined write/reads on the client side
	#[inline]
	pub fn get_client_writeread_endpoint(&self) -> Endpoint {
		Endpoint::from_cap(self.client_writeread_endpoint)
	}
	///Gets the endpoint for receiving messages on the client side
	#[inline]
	pub fn get_server_endpoint(&self) -> Endpoint {
		Endpoint::from_cap(self.server_endpoint)
	}
	///Gets the reply object
	#[inline]
	pub fn get_reply(&self) -> Reply {
		Reply::from_cap(self.reply)
	}
	///Gets the notification
	#[inline]
	pub fn get_notification(&self) -> Notification {
		Notification::from_cap(self.notification)
	}
	///Gets the access mode
	#[inline]
	pub fn get_access(&self) -> AccessMode {
		self.access
	}
	///Gets the errno (for notification FDs only)
	#[inline]
	pub fn get_errno(&self) -> ServerStatus {
		self.errno.load(Ordering::Relaxed) as ServerStatus
	}
	///Sets the errno (for notification FDs only)
	#[inline]
	pub fn set_errno(&self, errno: ServerStatus) {
		self.errno.store(errno as i64, Ordering::Relaxed)
	}
	///Gets the server receive flags
	#[inline]
	pub fn get_flags(&self) -> u64 {
		self.flags.load(Ordering::Relaxed)
	}
	///Sets the server receive flags
	#[allow(unused)]
	#[inline]
	pub fn set_flags(&self, flags: u64) {
		self.flags.store(flags, Ordering::Relaxed)
	}
	///Gets the control FD associated with this one
	#[inline]
	pub fn getctl(&self) -> RawFileDescriptor {
		self.control_fd
	}
}

impl Clone for BaseFileDescriptor {
	fn clone(&self) -> BaseFileDescriptor {
		BaseFileDescriptor {
			server_endpoint: self.server_endpoint,
			client_write_endpoint: self.client_write_endpoint,
			client_read_endpoint: self.client_read_endpoint,
			client_writeread_endpoint: self.client_writeread_endpoint,
			reply: self.reply,
			notification: self.notification,
			access: self.access,
			control_fd: self.control_fd,
			flags: AtomicU64::new(self.get_flags()),
			errno: AtomicI64::new(self.get_errno()),
		}
	}
}

//TODO: add a Thread FD type, which should wrap a TCB object in an RPC-like
//interface that allows setting/getting registers, suspending/resuming, and
//debugging a thread

//TODO: add an Interrupt FD type, which should wrap an IRQHandler and a
//Notification, allowing acknowledging the interrupt with writes and blocking
//on the interrupt notification with reads
///Unified container for `FileDescriptor` implementations
#[derive(Clone)]
#[repr(C)]
pub enum UnifiedFileDescriptor {
	Null,
	ChannelClient(ClientFileDescriptor),
	ChannelServer(ServerChannelFileDescriptor),
	ChannelServerMessage(ServerMessageDescriptor),
	Notification(NotificationFileDescriptor),
}

impl UnifiedFileDescriptor {
	///Creates a new client-side file descriptor
	pub fn new_client(endpoints: [Option<Endpoint>; 3], access: AccessMode)-> UnifiedFileDescriptor {
		let base_fd = ClientFileDescriptor::new(endpoints, access);
		UnifiedFileDescriptor::ChannelClient(base_fd)
	}
	///Creates a new server-side file descriptor
	pub fn new_server_channel(endpoint: Endpoint, access: AccessMode, options: ServerChannelOptions) -> UnifiedFileDescriptor {
		let base_fd = ServerChannelFileDescriptor::new(endpoint, access, options);
		UnifiedFileDescriptor::ChannelServer(base_fd)
	}
	pub fn new_server_message(reply: Reply) -> UnifiedFileDescriptor {
		UnifiedFileDescriptor::ChannelServerMessage(ServerMessageDescriptor::new(reply))
	}
	///Creates a new notification file descriptor
	pub fn new_notification(notification: Notification, access: AccessMode) -> UnifiedFileDescriptor {
		let base_fd = NotificationFileDescriptor::new(notification, access);
		UnifiedFileDescriptor::Notification(base_fd)
	}
	///Gets the underlying file descriptor object
	pub fn get_base_fd(&self) -> &dyn FileDescriptor {
		debug_println!("UnifiedFileDescriptor::get_base_fd: {}", self.index);
		match self {
			UnifiedFileDescriptor::ChannelClient(ref fd) => {
				fd
			},
			UnifiedFileDescriptor::ChannelServer(ref fd) => {
				fd
			},
			UnifiedFileDescriptor::ChannelServerMessage(ref fd) => {
				fd
			},
			UnifiedFileDescriptor::Notification(ref fd) => {
				fd
			},
			UnifiedFileDescriptor::Null => {
				&NULL_FD
			},
		}

	}
}

impl Default for UnifiedFileDescriptor {
	fn default() -> Self {
		UnifiedFileDescriptor::Null
	}
}

wrapper_impls!(UnifiedFileDescriptor);

///Public API for all file descriptor types
pub trait FileDescriptor {
	///Gets the default offset type for reads.
	///
	///The default implementation returns OffsetType::Current (the default
	///for clients)
	#[inline]
	fn default_read_offset_type(&self) -> OffsetType {
		OffsetType::Current
	}
	///Gets the default offset type for reads that update the stored
	///offset.
	///
	///The default implementation returns OffsetType::UpdateCurrent (the
	///default for clients)
	#[inline]
	fn default_read_update_offset_type(&self) -> OffsetType {
		OffsetType::UpdateCurrent
	}
	///Gets the default offset type for writes.
	///
	///The default implementation returns OffsetType::Current (the default
	///for clients)
	#[inline]
	fn default_write_offset_type(&self) -> OffsetType {
		OffsetType::Current
	}
	///Gets the default offset type for reads that update the stored
	///offset.
	///
	///The default implementation returns OffsetType::UpdateCurrent (the
	///default for clients)
	#[inline]
	fn default_write_update_offset_type(&self) -> OffsetType {
		OffsetType::UpdateCurrent
	}
	///Gets the reply object associated with this FD (if present). This is
	///only intended for internal use (regular code doesn't need to use
	///the reply directly)
	fn get_reply(&self) -> Option<Reply> {
		None
	}
	///Gets the access mode
	fn get_access(&self) -> AccessMode {
		AccessMode::ReadWrite
	}
	///Gets the thread-local fixed IPC buffer to use when reading from this
	///file descriptor. This should be called after every *readb or
	///*writereadb that returned a non-zero size to access the buffer
	///contents.
	///
	///IPC buffers are per-thread, not per-FD, and are overwritten on every
	///IPC transfer. The current implementation has two fixed IPC buffers
	///for each thread, selected automatically based on what type of message
	///was received, but threads must not depend on the buffer address
	///being consistent and must call it after every short read to get the
	///buffer address.
	///
	///This will return None if the last IPC call was not a read.
	///
	fn getbuf_r(&self) -> Option<FixedIPCBuffer> {
		None
	}
	///Gets the thread-local fixed IPC buffer to use when writing to this
	///file descriptor. This must be called before every *writeb or
	///*writereadb of non-zero size to access the buffer contents.
	///
	///IPC buffers are per-thread, not per-FD, and are overwritten on every
	///IPC transfer. The current implementation has two fixed IPC buffers
	///for each thread, selected automatically based on what type of message
	///was received, but threads must not depend on the buffer address
	///being consistent and must call it before every short write to get the
	///buffer address.
	///
	fn getbuf_w(&self) -> Option<FixedIPCBuffer> {
		None
	}
	///Internal method to set the errno returned on the next attempt to
	///access this FD.
	///
	///Currently only supported on notification FDs.
	fn seterrno(&self, _value: ServerStatus) {
	}
	///Seeks to the specified offset and reads from the file descriptor,
	///copying into the provided buffer.
	///
	///For client channels, this sends a read request to the server and
	///waits for the reply. All offset types are valid, but not all are
	///accepted by every server. The server's stored offset used by the
	///Current offset type is only updated if the Update* offset types are
	///used.
	///
	///For server message descriptors, this reads from the client's send
	///buffers into the provided buffer, and only the Start offset type is
	///accepted. No offset is stored.
	///
	///For server channel FDs, this reads a message from the channel, and
	///only works if a default message descriptor has been set for the
	///thread with setmd(). The offset type must be Current and the offset
	///must be 0. The status header is prepended to the message, and the
	///buffer must be at least the size of the header or InvalidArgument
	///will be returned. To split off the header into a separate buffer,
	///use a readv-family function instead. The default message descriptor
	///must not have a pending reply.
	///
	///For notification FDs, this waits for the notification to be
	///signalled, returning the notification word into the buffer. The
	///offset type must be Current or UpdateCurrent, and the offset must be
	///0. The buffer must be the size of a pointer (usize in Rust or size_t
	///in C).
	///
	///Returns the size and offset of the data that was read on success, or
	///an IOError on failure.
	///
	///Error variants returned:
	///
	///InvalidArgument:     offset or offset type invalid for the file
	///                     descriptor type
	///
	///                     buffer too small (for notifications and server
	///                     channels)
	///
	///InvalidOperation:    file descriptor set as default message
	///                     descriptor is some other type
	///
	///                     file descriptor is closed
	///
	///InvalidMessage:      invalid message received from IPC partner
	///ServerError:         server returned an error (only for client
	///                     channels)
	///
	///                     typical error codes:
	///
	///                     SRV_ERR_SRCH:   all server FDs closed (clients
	///                     		only)
	///
	///                     		server closed message descriptor
	///                     		with pending reply (clients
	///                     		only)
	///
	///                     SRV_ERR_BADF:   read attempted on write-only FD
	///
	///                    	SRV_ERR_INVAL:  invalid message received by
	///                    			server (internal error)
	///
	///                    	SRV_ERR_NOSYS:  invalid message type received
	///                    			by server (internal error)
	///
	///SyscallError:        a system call error occurred; usual values for
	///                     the specific error type include:
	///
	///                     InvalidLocalIPCAddress:
	///                     InvalidRemoteIPCAddress:
	///                     	a vector or buffer address provided by
	///                     	the local or remote thread respectively
	///                     	was outside of a valid user-level
	///                             mapping
	///                     ReplySequenceError:
	///                    		for message descriptors, no message was
	///                    		pending
	///                    		for server channels, the message
	///                    		descriptor already has a pending message
	///                     ThreadCancelled:
	///                             IPC was cancelled due to the thread
	///                             being suspended or deleted
	///
	///                     other syscall errors are usually internal errors
	///
	fn wpread(&self, buf: &mut [u8], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		let mut bufs = [RecvLongMsgBuffer::new(buf)];
		self.wpreadv(&mut bufs, offset, whence)
	}
	///Similar to wpread(), except the default offset type is used and no
	///offset is returned.
	///
	///The default offset type depends on the file descriptor type. For
	///client channels, it is UpdateCurrent. All other types have a single
	///supported offset type, which is used as their default.
	///
	///Error returns are similar to wpread().
	fn pread(&self, buf: &mut [u8], offset: Offset) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpread(buf, offset, self.default_read_offset_type()))
	}
	///Similar to wpread(), but splits the message across an array of
	///buffers rather than reading into a single buffer.
	///
	///The transport layer itself never combines multiple messages, although
	///some servers may of course combine multiple logical messages into a
	///single reply if the buffer is large enough.
	///
	///For server channels, the first buffer must be exactly the size of a
	///message header or InvalidArgument will be returned.
	///
	///Error returns are similar to wpread().
	fn wpreadv(&self, _bufs: &mut [RecvLongMsgBuffer], _offset: Offset, _whence: OffsetType) -> Result<(usize, Offset), IOError> { Err(IOError::InvalidOperation)
	}
	///Same as wpreadv(), except the default offset type is used and no
	///offset is returned.
	///
	///Error returns are the same as wpread().
	fn preadv(&self, bufs: &mut [RecvLongMsgBuffer], offset: Offset) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpreadv(bufs, offset, self.default_read_offset_type()))
	}
	///Similar to pread(), except the offset is always 0.
	///
	///Unlike pread, this updates the server-side stored offset for client
	///channels. Otherwise this is equivalent to pread with a 0 offset.
	///
	///Error returns are the same as wpread().
	fn read(&self, buf: &mut [u8]) -> Result<usize, IOError> {
		let mut bufs = [RecvLongMsgBuffer::new(buf)];
		self.readv(&mut bufs)
	}
	///Similar to read(), but splits the message across an array of
	///buffers rather than reading into a single buffer.
	///
	///Buffers are handled similarly to wpreadv().
	///
	///Error returns are the same as wpread().
	fn readv(&self, bufs: &mut [RecvLongMsgBuffer]) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpreadv(bufs, 0, self.default_read_update_offset_type()))
	}
	///Similar to read(), but accepts a server message descriptor for
	///accessing the send buffer and replying to the message.
	///
	///For server channels, the status header is prepended to the
	///message. To split off the header into a separate buffer, use a
	///readv-family function instead. The message descriptor must not have
	///a pending reply.
	///
	///This is equivalent to read() for FDs other than server channels.
	///The message descriptor must be -1 in this case or InvalidArgument
	///will be returned.
	///
	///Error returns are otherwise similar to wpread(), although the ones
	///related to message descriptors instead pertain to the specified
	///one and not the default.
	fn mread(&self, buf: &mut [u8], md: RawFileDescriptor) -> Result<usize, IOError> {
		let mut bufs = [RecvLongMsgBuffer::new(buf)];
		self.mreadv(&mut bufs, md)
	}
	///Similar to mread(), but splits the message across an array of
	///buffers rather than reading into a single buffer. The first buffer is
	///always used only for the header.
	///
	///Buffers are handled similarly to wpreadv().
	///
	///Error returns are the same as mread().
	fn mreadv(&self, bufs: &mut [RecvLongMsgBuffer], md: RawFileDescriptor) -> Result<usize, IOError> {
		if md == -1 {
			self.readv(bufs)
		}else{
			Err(IOError::InvalidArgument)
		}
	}
	///Same as wpread(), but uses the fixed IPC buffer instead of a
	///user-provided one.
	///
	///Additional errors not returned by wpread():
	///
	///MessageTooLong:      the specified message size is larger than the
	///                     fixed buffer
	///
	///Error returns are otherwise the same as wpread().
	fn wpreadb(&self, _size: usize, _offset: Offset, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		Err(IOError::InvalidOperation)
	}
	///Same as pread(), but uses the fixed IPC buffer instead of a
	///user-provided one.
	///
	///Error returns are the same as wpreadb()
	fn preadb(&self, size: usize, offset: Offset) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpreadb(size, offset, self.default_read_offset_type()))
	}
	///Reads a message into the fixed IPC buffer
	///
	///Semantics are otherwise the same as read()
	///Error returns are the same as wpreadb()
	fn readb(&self, size: usize) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpreadb(size, 0, self.default_read_update_offset_type()))
	}
	///Reads a message into the fixed IPC buffer, accepting a server
	///message descriptor.
	///
	///Semantics are otherwise the same as mread()
	///Error returns are the same as wpreadb(), although the ones related to
	///message descriptors instead pertain to the provided one and not the
	///default one.
	fn mreadb(&self, size: usize, md: RawFileDescriptor) -> Result<usize, IOError> {
		if md == -1 {
			self.readb(size)
		}else{
			Err(IOError::InvalidArgument)
		}
	}
	///Seeks to the specified offset.
	///
	///Returns the new current offset on success, or the size and offset of
	///the server-specific error status and an IOError on failure.
	///
	///This is only valid for client-side channel FDs, and is equivalent to
	///wpreadb with a zero size and the Update flag set.
	///
	///Error returns are the same as wpread()
	fn seek(&self, _offset: Offset, _whence: OffsetType) -> Result<Offset, IOError> {
		Err(IOError::InvalidOperation)
	}
	///Seeks to the specified offset and writes data to the file descriptor
	///from a user-provided buffer
	///
	///For client channel FDs, this sends a write request to the server and
	///waits for the response. All offset types are valid, but not all
	///servers accept all offset types. The server's stored offset is not
	///updated unless one of the Update* offset types is used.
	///
	///For server message descriptors, the Start offset type writes to the
	///specified offset in the client's send buffer. The End offset type
	///will end the reply, sending it to the client and freeing the message
	///descriptor for use in future mread* calls. The offset in this case is
	///returned to the client as is. If a data buffer is included when
	///ending a reply, it will be written to the beginning of the client's
	///reply buffer, overwriting any data previously written there.
	///
	///For notification FDs, this signals the notification. The offset must
	///be zero and the offset type must be Current or UpdateCurrent.
	///
	///This is not supported for server channel FDs; use this method on
	///server message descriptors instead on the server side.
	///
	///Error returns are much the same as for wpread, although SRV_ERR_BADF
	///is returned if this is called on a read-only file descriptor, not
	///a write-only one.
	fn wpwrite(&self, buf: &[u8], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		let bufs = [SendLongMsgBuffer::new(buf)];
		self.wpwritev(&bufs, offset, whence)
	}
	///Similar to wpwrite(), except the default offset type is used and no
	///offset is returned.
	///
	///The default offset type depends on the file descriptor type. For
	///client channels, it is Current (which writes to the current offset).
	///For server message descriptors, it is End (which sends the reply to
	///the client). All other types have a single supported offset type,
	///which is used as their default.
	///
	///Error returns are the same as wpwrite().
	fn pwrite(&self, buf: &[u8], offset: Offset) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpwrite(buf, offset, self.default_write_offset_type()))
	}
	///Same as wpwrite(), but splits the message across an array of
	///buffers rather than writing from a single buffer.
	///
	///Error returns are the same as wpwrite().
	fn wpwritev(&self, _bufs: &[SendLongMsgBuffer], _offset: Offset, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		Err(IOError::InvalidOperation)
	}
	///Same as wpwritev(), except the default offset type is used and no
	///offset is returned.
	///
	///Error returns are the same as wpwrite().
	fn pwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpwritev(bufs, offset, self.default_write_offset_type()))
	}
	///Similar to wpwrite(), but accepts an additional status argument to
	///return to the client when used on server message descriptors.
	///
	///The status is treated as the accepted size for writes if zero or
	///greater, or the errno to return to the client if less than zero. For
	///replies to reads, the status is only returned to the client if
	///negative and a positive value only indicates success (the actual size
	///returned is determined by the buffer).
	///
	///For everything other than message descriptors, the status is required
	///to be zero (otherwise InvalidArgument will be returned). Error
	///returns are otherwise the same as for wpwrite().
	fn mwpwrite(&self, buf: &[u8], offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
		let bufs = [SendLongMsgBuffer::new(buf)];
		self.mwpwritev(&bufs, offset, whence, status)
	}
	///Similar to mwpwrite(), but splits the message across an array of
	///buffers rather than writing from a single buffer.
	///
	///Error returns are the same as mwpwrite().
	fn mwpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
		if status == 0 {
			self.wpwritev(bufs, offset, whence)
		}else{
			Err(IOError::InvalidArgument)
		}
	}
	///Similar to mwpwrite(), but writes from the fixed buffer instead of a
	///user-provided buffer.
	///
	///Additional errors not returned by mwpwrite():
	///
	///MessageTooLong:      the specified message size is larger than the
	///                     fixed buffer
	///
	///Error returns are otherwise the same as mwpwrite().
	fn mwpwriteb(&self, len: usize, offset: Offset, whence: OffsetType, status: ServerStatus) -> Result<(usize, Offset), IOError> {
		if status == 0 {
			self.wpwriteb(len, offset, whence)
		}else{
			Err(IOError::InvalidOperation)
		}

	}
	///Similar to pwrite, except the offset is always 0.
	///
	///Unlike pwrite, this updates the server-side stored offset for client
	///channels. Otherwise this is equivalent to pwrite with a 0 offset.
	///
	///For server message descriptors, this writes to the start of the
	///client's receive buffer, and for client-side FDs, the server will
	///use its saved offset (i.e. it is equivalent to pwrite with 0 as
	///the offset, with UpdateCurrent as the offset type for client channels
	///and Start for server message descriptors).
	///
	///Error returns are otherwise the same as pwrite().
	fn write(&self, buf: &[u8]) -> Result<usize, IOError> {
		let bufs = [SendLongMsgBuffer::new(buf)];
		self.writev(&bufs)
	}
	///Similar to write(), but writes from an array of buffers instead of a
	///single buffer.
	///
	///Error returns are the same as write().
	fn writev(&self, bufs: &[SendLongMsgBuffer]) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpwritev(&bufs, 0, self.default_write_update_offset_type()))
	}
	///Similar to wpwrite(), but writes from the fixed IPC buffer instead of
	///a user-provided buffer.
	///
	///Additional errors not returned by wpwrite():
	///
	///MessageTooLong:      the specified message size is larger than the
	///                     fixed buffer
	///
	///Error returns are otherwise the same as wpwrite().
	fn wpwriteb(&self, _size: usize, _offset: Offset, _whence: OffsetType) -> Result<(usize, Offset), IOError> {
		Err(IOError::InvalidOperation)
	}
	///Similar to pwrite(), but writes from the fixed IPC buffer instead of
	///a user-provided buffer.
	///
	///Error returns are the same as wpwriteb().
	fn pwriteb(&self, size: usize, offset: Offset) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpwriteb(size, offset, self.default_write_offset_type()))
	}
	///Same as write(), but writes from the fixed IPC buffer instead of a
	///user-provided buffer.
	///
	///Error returns are the same as wpwriteb().
	fn writeb(&self, size: usize) -> Result<usize, IOError> {
		convert_from_offset_result(self.wpwriteb(size, 0, self.default_write_update_offset_type()))
	}
	///This is equivalent to a write() followed by a read() combined into a
	///single system call. This is supported on client and server channels
	///only and will fail on other file descriptor types.
	///
	///On client channels, both read and write phases are done with
	///a zero offset and UpdateCurrent as the offset type.
	///
	///On server channels, this sends the reply through the thread's default
	///message descriptor with a zero offset and status.
	///
	///Buffers are handled similarly to wpwritev() and wpreadv().
	///
	///Error returns are similar to write() and read().
	fn writeread(&self, w_buf: &[u8], r_buf: &mut [u8]) -> Result<(usize, usize), IOError> {
		self.wpwriteread(w_buf, r_buf, 0, OffsetType::UpdateCurrent)
	}
	///Similar to writeread(), but splits both messages across arrays of
	///buffers.
	///
	///Buffers are handled similarly to wpwritev() and wpreadv().
	///
	///Error returns are similar to writeread().
	fn writereadv(&self, w_buf: &[SendLongMsgBuffer], r_buf: &mut [RecvLongMsgBuffer]) -> Result<(usize, usize), IOError> {
		self.wpwritereadv(w_buf, r_buf, 0, OffsetType::UpdateCurrent)
	}
	///Same as writeread(), but uses the fixed IPC buffer instead of a
	///user-provided buffer.
	///
	///Error returns are similar to writeb() and readb().
	fn writereadb(&self, w_len: usize, r_len: usize) -> Result<(usize, usize), IOError> {
		self.wpwritereadb(w_len, r_len, 0, OffsetType::UpdateCurrent)
	}
	///Similar to writeread(), but accepts an offset. Typically the same
	///offset will be used for both the read and the write, but this is
	///server-dependent.
	fn pwriteread(&self, w_buf: &[u8], r_buf: &mut [u8], offset: Offset) -> Result<(usize, usize), IOError> {
		self.wpwriteread(w_buf, r_buf, offset, OffsetType::Start)
	}
	///Similar to pwriteread(), but splits both messages across arrays of
	///buffers.
	///
	///Buffers are handled similarly to wpwritev() and wpreadv().
	///
	///Error returns are similar to writeread().
	fn pwritereadv(&self, w_buf: &[SendLongMsgBuffer], r_buf: &mut [RecvLongMsgBuffer], offset: Offset) -> Result<(usize, usize), IOError> {
		self.wpwritereadv(w_buf, r_buf, offset, OffsetType::Start)
	}
	///Same as pwriteread(), but uses the fixed IPC buffer instead of a
	///user-provided buffer.
	///
	///Error returns are similar to writeb() and readb().
	fn pwritereadb(&self, w_len: usize, r_len: usize, offset: Offset) -> Result<(usize, usize), IOError> {
		self.wpwritereadb(w_len, r_len, offset, OffsetType::Start)
	}
	///Similar to pwriteread(), but accepts an offset type.
	fn wpwriteread(&self, w_buf: &[u8], r_buf: &mut [u8], offset: Offset, whence: OffsetType) -> Result<(usize, usize), IOError> {
		let w_bufs = [SendLongMsgBuffer::new(w_buf)];
		let mut r_bufs = [RecvLongMsgBuffer::new(r_buf)];
		self.wpwritereadv(&w_bufs, &mut r_bufs, offset, whence)
	}
	///Similar to wpwriteread(), but splits both messages across arrays of
	///buffers.
	///
	///Buffers are handled similarly to wpwritev() and wpreadv().
	///
	///Error returns are similar to writeread().
	fn wpwritereadv(&self, _w_buf: &[SendLongMsgBuffer], _r_buf: &mut [RecvLongMsgBuffer], _offset: Offset, _whence: OffsetType) -> Result<(usize, usize), IOError> {
		Err(IOError::InvalidOperation)
	}
	///Same as wpwriteread(), but uses the fixed IPC buffer instead of a
	///user-provided buffer.
	///
	///Error returns are similar to writeb() and readb().
	fn wpwritereadb(&self, _w_len: usize, _r_len: usize, _offset: Offset, _whence: OffsetType) -> Result<(usize, usize), IOError> {
		Err(IOError::InvalidOperation)
	}
	///Similar to writeread(), but accepts a server message descriptor,
	///sending a reply through it and then accepting a new message into
	///it, as well as an offset and status to return to the client.
	///
	///The message descriptor must have a reply pending.
	///
	///The offset, accepted size, and message descriptor parameters are only
	///valid for server channels. For all other FDs, the offset must be
	///zero, the status must be 0, and the message descriptor must be -1
	///(otherwise InvalidOperation will be returned). This is true of all
	///other m* functions as well.
	///
	///Error returns are similar to mwpwrite() and read().
	fn mpwriteread(&self, w_buf: &[u8], w_offset: Offset, r_buf: &mut [u8], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		let w_bufs = [SendLongMsgBuffer::new(w_buf)];
		let mut r_bufs = [RecvLongMsgBuffer::new(r_buf)];
		self.mpwritereadv(&w_bufs, w_offset, &mut r_bufs, status, md)
	}
	///Same as mpwriteread(), but splits both messages across arrays of
	///buffers.
	///
	///Buffers are handled similarly to wpwritev() and wpreadv().
	///
	///Error returns are similar to mpwriteread().
	fn mpwritereadv(&self, w_bufs: &[SendLongMsgBuffer], w_offset: Offset,  r_bufs: &mut [RecvLongMsgBuffer], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		if md == -1 && w_offset == 0 && status == 0 {
			match self.writereadv(w_bufs, r_bufs) {
				Ok((w_len, r_len)) => Ok((w_len, r_len)),
				Err(err) => Err(err),
			}
		}else{
			Err(IOError::InvalidArgument)
		}
	}
	///Same as mpwriteread(), but uses the fixed buffer instead of a
	///user-provided buffer.
	///
	///Error returns are similar to mwpwriteb() and readb().
	fn mpwritereadb(&self, w_len: usize, w_offset: Offset, r_len: usize, status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		if md == -1 && w_offset == 0 && status == 0 {
			match self.writereadb(w_len, r_len) {
				Ok((w_len, r_len)) => Ok((w_len, r_len)),
				Err(err) => Err(err),
			}
		}else{
			Err(IOError::InvalidArgument)
		}
	}
	///Same as mpwriteread() with a zero offset value.
	///
	///Error returns are similar to mpwriteread().
	fn mwriteread(&self, w_buf: &[u8], r_buf: &mut [u8], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		self.mpwriteread(w_buf, 0, r_buf, status, md)
	}
	///Same as mwriteread(), but splits both messages across arrays of
	///buffers.
	///
	///Buffers are handled similarly to wpwritev() and wpreadv().
	///
	///Error returns are similar to mpwriteread().
	fn mwritereadv(&self, w_buf: &[SendLongMsgBuffer], r_buf: &mut [RecvLongMsgBuffer], status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		self.mpwritereadv(w_buf, 0, r_buf, status, md)
	}
	///Equivalent to writereadb(), but accepts a server message
	///descriptor.
	///
	///Error returns are similar to mwpwriteb() and readb().
	fn mwritereadb(&self, w_len: usize, r_len: usize, status: ServerStatus, md: RawFileDescriptor) -> Result<(usize, usize), IOError> {
		self.mpwritereadb(w_len, 0, r_len, status, md)
	}
	///Gets the control file descriptor associated with this file
	///descriptor. Only valid on client channels.
	fn getctl(&self) -> Result<RawFileDescriptor, IOError>{
		Err(IOError::InvalidOperation)
	}
	///Sets the control file descriptor associated with this file
	///descriptor. Only valid on client channels.
	fn setctl(&self, _fd: RawFileDescriptor) -> Result<(), IOError>{
		Err(IOError::InvalidOperation)
	}
	///Internal method called when all file descriptors on the other side of
	///a file description have been closed.
	fn clunk(&self) {
	}
}
