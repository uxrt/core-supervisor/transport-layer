// Copyright 2022-2025 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use sel4::{
	CAP_NULL,
	Endpoint,
	ErrorDetails,
	LONG_IPC_OPTIONAL,
	ToCap,
};

use sel4_sys::{
	seL4_GetIPCBuffer,
	seL4_NumErrors,
};

use crate::{
	AccessMode,
	FileDescriptor,
	IOError,
	FixedIPCBuffer,
	MsgType,
	Offset,
	OffsetType,
	RawFileDescriptor,
	RecvLongMsgBuffer,
	SendLongMsgBuffer,
	ServerStatus,
	get_long_recv_buf,
	get_long_send_buf,
};

#[allow(unused)]
use crate::debug_println;

use crate::channel::{
	BaseChannelFileDescriptor,
	ClientLabel,
	CLIENT_ENDPOINT_COPIES,
	RESERVED_REGS,
	OFFSET_IDX,
	ReadBufType,
	SIZE_IDX,
	ServerLabel,
	get_primary_buffer,
	get_secondary_buffer,
	get_send_word_size,
};

use core::mem::size_of;

///A client-side file descriptor.
#[derive(Clone)]
#[repr(C)]
pub struct ClientFileDescriptor {
	base_fd: BaseChannelFileDescriptor,
}

impl ClientFileDescriptor {
	///Internal constructor (UnifiedFileDescriptor::new_client is the public
	///one)
	pub(crate) fn new(endpoints: [Option<Endpoint>; CLIENT_ENDPOINT_COPIES], access: AccessMode) -> ClientFileDescriptor {
		debug_println!("ClientFileDescriptor::new: {:?}", endpoints);
		ClientFileDescriptor {
			base_fd: BaseChannelFileDescriptor::new(access, endpoints[0], endpoints[1], endpoints[2], None, 0),
		}
	}
	///Internal wrapper around seL4_Call(). This is the core of all client
	///IPC methods.
	fn call(&self, msgtype: MsgType, send_short_size: usize, recv_short_size: usize, offset: Offset, whence: OffsetType, send_long_bufs: Option<&[SendLongMsgBuffer]>, recv_long_bufs: Option<&mut [RecvLongMsgBuffer]>, long_optional: bool) -> (usize, usize, usize, usize, Result<(), IOError>) {
		debug_println!("ClientFileDescriptor::call: {:?} {} {} {} {:?}", msgtype, send_short_size, recv_short_size, offset, whence);
		let fix_buf = unsafe { &mut*(seL4_GetIPCBuffer()) };

		let (mut send_word_size, has_footer) = match get_send_word_size(send_short_size, offset, 0) {
			Ok((s, f)) => (s, f),
			Err(err) => {
				return (0, 0, 0, 0, Err(err));
			},
		};

		debug_println!("ClientFileDescriptor::call: offset: {}", offset);
		if has_footer {
			debug_println!("has footer");
			fix_buf.msg[send_word_size + OFFSET_IDX] = offset as usize;
			send_word_size += RESERVED_REGS;
		}

		let label = ClientLabel::pack(whence, recv_short_size, send_short_size % size_of::<usize>(), has_footer);
		let endpoint = match msgtype {
			MsgType::Write => self.base_fd.get_client_write_endpoint(),
			MsgType::Read => self.base_fd.get_client_read_endpoint(),
			MsgType::WriteRead => self.base_fd.get_client_writeread_endpoint(),
			_ => { return (0, 0, 0, 0, Err(IOError::InvalidOperation)) },
		};
		if endpoint.to_cap() == CAP_NULL {
			//if the cap is null, this is very likely an attempt to
			//perform an operation on it that's not permitted
			return (0, 0, 0, 0, Err(IOError::InvalidOperation));
		}
		let flags = if long_optional {
			LONG_IPC_OPTIONAL
		}else{
			0
		};

		let res = endpoint.long_call(label, send_word_size, send_long_bufs.unwrap_or(&[]), recv_long_bufs.unwrap_or(&mut []), flags);
		let msg = match res {
			Ok(m) => m,
			Err(err) => {
				let err = if let Some(ErrorDetails::InvalidCapability { which: _ }) = err.details() {
					(0, 0, 0, 0, Ok(()))
				}else{
					(0, 0, 0, 0, Err(IOError::SyscallError(err)))
				};
				return err;
			},
		};
		let mut recv_word_size = msg.words_transferred();
		debug_println!("recv_word_size: {}", recv_word_size);
		debug_println!("label: {}", msg.label);
		let (server_error, w_short_size, recv_remainder, has_footer) = ServerLabel::unpack(msg.label);
		debug_println!("recv_word_size: {} has_footer: {:?}", recv_word_size, has_footer);
		let (w_size, offset) = if has_footer {
			if recv_word_size < RESERVED_REGS {
				return (0, 0, 0, 0, Err(IOError::InvalidMessage));
			}
			recv_word_size -= RESERVED_REGS;
			(fix_buf.msg[recv_word_size + SIZE_IDX],
			fix_buf.msg[recv_word_size + OFFSET_IDX])
		}else{
			(w_short_size, 0)
		};

		let mut res = Ok(());
		if server_error > 0 {
			res = Err(IOError::ServerError(server_error as ServerStatus - seL4_NumErrors as ServerStatus));
		}

		if recv_word_size > 0 && recv_remainder > 0 {
			recv_word_size -= 1;
		}

		debug_println!("recv_len: {} recv_total_len: {} recv_reply_len: {}", msg.recv_len(), msg.recv_total_len(), msg.recv_reply_len());

		(w_size, recv_word_size * size_of::<usize>() + recv_remainder, offset, msg.recv_len(), res)

	}
	///Internal method to return a value based on the result of call()
	fn return_val(val0: u64, val1: u64, res: Result<(), IOError>) -> Result<(u64, u64), IOError> {
		match res {
			Ok(_) => {
				Ok((val0, val1))
			},
			Err(err) => {
				if err.is_clunk() {
					Ok((0, 0))
				}else{
					Err(err)
				}
			}
		}
	}
	fn return_val_offset(size: u64, offset: u64, res: Result<(), IOError>) -> Result<(usize, Offset), IOError> {
		let (ret_size, ret_offset) = Self::return_val(size, offset, res)?;
		Ok((ret_size as usize, ret_offset as Offset))
	}
	fn return_val_size(size0: u64, size1: u64, res: Result<(), IOError>) -> Result<(usize, usize), IOError> {
		let (ret_size0, ret_size1) = Self::return_val(size0, size1, res)?;
		Ok((ret_size0 as usize, ret_size1 as usize))
	}
}

impl FileDescriptor for ClientFileDescriptor {
	#[inline]
	fn get_access(&self) -> AccessMode {
		self.base_fd.get_access()
	}
	#[inline]
	fn getbuf_r(&self) -> Option<FixedIPCBuffer> {
		self.base_fd.getbuf_r()
	}
	fn getbuf_w(&self) -> Option<FixedIPCBuffer> {
		self.base_fd.getbuf_w()
	}
	fn wpreadv(&self, bufs: &mut [RecvLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		let (_, _, offset, long_size, res) = self.call(MsgType::Read, 0, 0, offset, whence, None, Some(bufs), false);
		self.base_fd.set_read_type(ReadBufType::Unset);
		Self::return_val_offset(long_size as u64, offset as u64, res)
	}
	fn wpreadb(&self, size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		let mut fixed_buf = get_secondary_buffer().get_data();
		let mut long_bufs = get_long_recv_buf(&mut fixed_buf, size)?;
		let (_, short_size, offset, long_size, res) = self.call(MsgType::Read, 0, size, offset, whence, None, Some(&mut long_bufs), true);
		let size = self.base_fd.handle_fixed_read_bufs(short_size, long_size);
		Self::return_val_offset(size as u64, offset as u64, res)
	}
	fn seek(&self, offset: Offset, whence: OffsetType) -> Result<Offset, IOError> {
		match self.wpreadb(0, offset, whence.update()) {
			Ok((_, offset)) => Ok(offset),
			Err(err) => Err(err),
		}
	}
	fn wpwritev(&self, bufs: &[SendLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		let (size, _, offset, _, res) = self.call(MsgType::Write, 0, 0, offset, whence, Some(bufs), None, false);
		Self::return_val_offset(size as u64, offset as u64, res)
	}
	fn wpwriteb(&self, size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, Offset), IOError>{
		let buf = self.getbuf_w().unwrap().get_data();
		let long_bufs = get_long_send_buf(&buf, size)?;
		let (size, _, offset, _, res) = self.call(MsgType::Write, size, 0, offset, whence, Some(&long_bufs), None, true);
		Self::return_val_offset(size as u64, offset as u64, res)
	}
	fn wpwritereadv(&self, w_bufs: &[SendLongMsgBuffer], r_bufs: &mut [RecvLongMsgBuffer], offset: Offset, whence: OffsetType) -> Result<(usize, usize), IOError> {
		let (w_size, _, _, r_long_size, res) = self.call(MsgType::WriteRead, 0, 0, offset, whence, Some(w_bufs), Some(r_bufs), false);
		debug_println!("wpwritereadv: w_size: {} r_long_size: {}", w_size, r_long_size);
		Self::return_val_size(w_size as u64, r_long_size as u64, res)
	}
	fn wpwritereadb(&self, w_size: usize, mut r_size: usize, offset: Offset, whence: OffsetType) -> Result<(usize, usize), IOError> {
		//for a writereadb(), no offset is returned and that field
		//is instead used as a size field in order to not require
		//using extra message registers; writeread() is mostly
		//intended for non-storage-like files with complex
		//request/response semantics, so they don't need offsets
		let w_fixed_buf = get_primary_buffer().get_data();
		let w_long_bufs = get_long_send_buf(&w_fixed_buf, w_size)?;

		let mut r_fixed_buf = get_secondary_buffer().get_data();
		let mut r_long_bufs = get_long_recv_buf(&mut r_fixed_buf, r_size)?;
		let (w_size, r_short_size, _, r_long_size, res) = self.call(MsgType::WriteRead, w_size, r_size, offset, whence, Some(&w_long_bufs), Some(&mut r_long_bufs), true);
		r_size = self.base_fd.handle_fixed_read_bufs(r_short_size, r_long_size);
		Self::return_val_size(w_size as u64, r_size as u64, res)
	}
	fn getctl(&self) -> Result<RawFileDescriptor, IOError> {
		let fd = self.base_fd.getctl();
		if fd > -1 {
			Ok(fd)
		}else{
			Err(IOError::InvalidOperation)
		}
	}
}

///Sends a clunk message when no more shared clients are left
pub fn shared_client_clunk(endpoint: Endpoint, client_id: RawFileDescriptor){
	let buf = unsafe { &mut*(seL4_GetIPCBuffer()) };
	buf.msg[0] = client_id as usize;
	let _ = endpoint.call(0, 1, 0);
}
